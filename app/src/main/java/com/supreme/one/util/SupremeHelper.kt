package com.supreme.one.util

import android.annotation.SuppressLint
import android.app.Activity
import com.google.android.material.bottomnavigation.BottomNavigationView
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView


/**
 * Created by Charlton on 2/7/18.
 */
object SupremeHelper {
    private var last_layout_id = 0

    fun bitShift(message: String, shift: Int): String {
        var codedMsg = ""
        for (i in 0 until message.length) {
            codedMsg += (message[i].toInt() xor shift).toChar()
        }
        return codedMsg
    }

    @SuppressLint("RestrictedApi")
    fun disableShiftMode(view: BottomNavigationView) {
        val menuView = view.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for (i in 0 until menuView.childCount) {
                val item = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShifting(false)
                // set once again checked value, so view will be updated
                item.setChecked(item.itemData.isChecked)
            }
        } catch (e: NoSuchFieldException) {
            Log.e("BNVHelper", "Unable to get shift mode field", e)
        } catch (e: IllegalAccessException) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e)
        }

    }

    @SuppressLint("CommitTransaction")
    fun show(activity: Activity, layout_id: Int) {
        if (last_layout_id == 0) {
            last_layout_id = layout_id
            activity.findViewById<FrameLayout>(layout_id).visibility = View.VISIBLE
        } else {
            activity.findViewById<FrameLayout>(last_layout_id).visibility = View.INVISIBLE
            activity.findViewById<FrameLayout>(layout_id).visibility = View.VISIBLE
            last_layout_id = layout_id
        }
    }

    class Color(val red: Int, val blue: Int, val green: Int)

    fun getColor(RGBint: Int): Color {
        return Color(android.graphics.Color.red(RGBint), android.graphics.Color.green(RGBint), android.graphics.Color.blue(RGBint))
    }

    fun getAvatarUrlFromId(fromPlayerId: Int): String {
        val userId = fromPlayerId.toString()
        val from = 0
        val to = userId.length - 3
        if (to > -1) {
            val substring = userId.substring(from, to)
            val truncated = substring.reversed()
            /**
             * 000/000/000/id
             */
            val arr = ArrayList<String>()

            for (i in 0..(((truncated.length / 3) + if (truncated.length % 3 > 0) 1 else 0))) {
                var string = ""
                for (j in 0..2) {
                    val position = (i * 3) + j
                    try {
                        string += truncated[position]
                    } catch (e: Exception) {

                    }
                }
                if (!string.isEmpty()) {
                    arr.add(string.reversed())
                }
            }
            when (arr.size) {
                3 -> {
                    if (arr[2].length == 2) {
                        arr[2] = "0" + arr[2]
                    } else if (arr[2].length == 1) {
                        arr[2] = "00" + arr[2]
                    }

                }
                2 -> {
                    if (arr[1].length == 2) {
                        arr[1] = "0" + arr[1]
                    } else if (arr[1].length == 1) {
                        arr[1] = "00" + arr[1]
                    }
                    arr.add("000")

                }
                1 -> {
                    if (arr[0].length == 2) {
                        arr[0] = "0" + arr[0]
                    } else if (arr[0].length == 1) {
                        arr[0] = "00" + arr[0]
                    }
                    arr.add("000")
                    arr.add("000")
                }
                0 -> {
                    arr.add("000")
                    arr.add("000")
                    arr.add("000")
                }
            }
            arr.reverse()
            val calculatedString = arr.joinToString("/")
            return String.format("https://yw-web.yoworld.com/user/images/yo_avatars/%s/%s.png", calculatedString, fromPlayerId)
        } else {
            return String.format("https://yw-web.yoworld.com/user/images/yo_avatars/000/000/000/%s.png", fromPlayerId)
        }

    }
}