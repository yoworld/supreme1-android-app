package com.supreme.one.util

import android.content.SharedPreferences
import com.google.gson.Gson
import com.supreme.one.network.models.SupremeAccount
import com.supreme.one.network.models.UserModel
import timber.log.Timber

/**
 * Created by Charlton on 1/2/18.
 */
class SharedPreferenceManagerImpl(private val sharedPreferences: SharedPreferences) : SharedPreferenceManager {
    override fun saveRoomId(roomId: Int) {
        val edit = sharedPreferences.edit()
        edit.putInt(ROOM_ID, roomId)
        edit.apply()
    }

    override fun getRoomId(): Int {
        return sharedPreferences.getInt(ROOM_ID, 0)
    }

    override fun saveInstanceId(instanceId: Int) {
        val edit = sharedPreferences.edit()
        edit.putInt(INSTANCE_ID, instanceId)
        edit.apply()
    }

    override fun getInstanceId(): Int {
        return sharedPreferences.getInt(INSTANCE_ID, 0)
    }

    override fun setAccountId(id: Int) {
        val edit = sharedPreferences.edit()
        edit.putInt(USER_ACCOUNT_ID, id)
        edit.apply()
        Timber.e("LOGGED USER ACCOUNT ID: %s", id)
    }

    override fun getAccountId(): Int {
        return sharedPreferences.getInt(USER_ACCOUNT_ID, 0)
    }

    override fun getAccountToken(): String? {
        return sharedPreferences.getString(USER_ACCOUNT_TOKEN, null)
    }

    override fun setAccountToken(token: String) {
        val edit = sharedPreferences.edit()
        edit.putString(USER_ACCOUNT_TOKEN, token)
        edit.apply()
        Timber.e("LOGGED ACTIVE ACCOUNT: %s", token)
    }

    override fun saveId(user_id: Int) {
        val edit = sharedPreferences.edit()
        edit.putInt(USER_ID, user_id)
        edit.apply()
        Timber.e("LOGGED USER ID: %s", user_id)
    }

    override fun getId(): Int {
        return sharedPreferences.getInt(USER_ID, 0)
    }

    override fun isLoggedIn(): Boolean {
        return getUser() != null && getToken() != null
    }


    override fun getUser(): UserModel? {
        val userJson = sharedPreferences.getString(USER, null)
        return if (!userJson.isNullOrEmpty()) Gson().fromJson(userJson, UserModel::class.java) else null
    }

    override fun saveUser(user: UserModel?) {
        try {
            val edit = sharedPreferences.edit()
            val userJson = Gson().toJson(user)
            edit.putString(USER, userJson)
            edit.apply()
            Timber.e("LOGGED USER JSON: %s", userJson)
        }catch(e: Exception){
            Timber.e(e, "Error")
        }
    }

    override fun saveToken(string: String?){
        try {

            val edit = sharedPreferences.edit()
            edit.putString(USER_TOKEN, string)
            edit.apply()

            Timber.e("SAVED USER USER TOKEN: %s", string)
        }catch (e: Exception){
            Timber.e(e, "Error2")

        }
    }

    override fun getToken(): String? {
        return sharedPreferences.getString(USER_TOKEN, null)
    }

    override fun logout() {
        val edit = sharedPreferences.edit()
        edit.clear()
        edit.apply()
    }

    companion object {
        private const val ROOM_ID: String = "com.supreme.one.ROOM.ID"
        private const val INSTANCE_ID: String = "com.supreme.one.INSTANCE.ID"
        private const val USER: String = "com.supreme.one.USER"
        private const val USER_ID: String = "com.supreme.on.USER.ID"
        private const val USER_TOKEN: String = "com.supreme.one.USER.TOKEN"
        private const val USER_ACCOUNT: String = "com.supreme.one.USER.ACCOUNT"
        private const val USER_ACCOUNT_ID: String = "com.supreme.one.USER.ACCOUNT.ID"
        private const val USER_ACCOUNT_TOKEN: String = "com.supreme.one.USER.ACCOUNT.USER_TOKEN"

    }
}