package com.supreme.one.util

import com.supreme.one.network.models.UserModel

/**
 * Created by Charlton on 1/2/18.
 */

interface SharedPreferenceManager {
    fun getUser(): UserModel?
    fun logout(): Unit
    fun saveUser(user: UserModel?)
    fun saveId(user_id: Int)
    fun getId(): Int
    fun saveToken(string: String?)
    fun getToken(): String?
    fun isLoggedIn(): Boolean
    fun getAccountToken(): String?
    fun setAccountToken(token: String): Unit
    fun setAccountId(id: Int)
    fun getAccountId(): Int
    fun saveRoomId(roomId: Int)
    fun getRoomId() :Int
    fun saveInstanceId(instanceId: Int)
    fun getInstanceId(): Int

}
