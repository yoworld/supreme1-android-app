package com.supreme.one.util

import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber

/**
 * Created by Charlton on 3/1/18.
 */
class AuthInterceptor(private var mSharedPreferenceManager: SharedPreferenceManager, private var isUserToken: Boolean) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val original = chain.request()
            val request = original.newBuilder()
            Timber.e("SETTING CUSTOM HEADERS")
            if (mSharedPreferenceManager.getToken() != null && !original.url.encodedPath.startsWith("/api/v1/auth")) {
                val apiToken = if (isUserToken) mSharedPreferenceManager.getToken() else mSharedPreferenceManager.getAccountToken()
                request.addHeader("Authorization", String.format("Bearer %s", apiToken))
            }
            Timber.e("HEADERS SET")
            request.url(original.url)
            request.method(original.method, original.body)
            return chain.proceed(request.build())
        } catch (e: Exception) {
            Timber.e(e, "Something went wrong")
            return chain.proceed(chain.request())
        }
    }

}