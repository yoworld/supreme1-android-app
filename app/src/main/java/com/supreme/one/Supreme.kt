package com.supreme.one

import androidx.annotation.StringDef
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.client.BigBrotherResponse
import com.supreme.one.network.client.models.*
import com.supreme.one.network.models.SoundCloudResult
import com.supreme.one.network.models.UserModel
import com.supreme.one.network.models.YoutubeResult
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject

/**
 * Created by Charlton on 2/8/18.
 */

object Supreme {

    private const val INSTANCE_ADDRESS = "supreme.us1a.cloud.realm.io"
    const val AUTH_URL = "https://$INSTANCE_ADDRESS/auth"
    const val REALM_BASE_URL = "realm://$INSTANCE_ADDRESS"
    val STICKERS: HashMap<String, Int> = object: HashMap<String, Int>(){
        init {
            put("1", R.drawable.smile2emoticon)
            put("2", R.drawable.angryemoticon)
            put("3", R.drawable.cryemoticon)
            put("4", R.drawable.droolemoticon)
            put("5", R.drawable.weirdoemoticon)
            put("6", R.drawable.mortifiedemoticon)
            put("7", R.drawable.happyemoticon)

            put("9", R.drawable.hurtemoticon)
            put("10", R.drawable.loveemoticon)
            put("11", R.drawable.loveloveemoticon)
            put("12", R.drawable.maniacalemoticon)
            put("13", R.drawable.worried2emoticon)
            put("14", R.drawable.pleasedemoticon)
            put("15", R.drawable.duhemoticon)
            put("16", R.drawable.sleepemoticon)
            put("17", R.drawable.kissemoticon)
            put("18", R.drawable.mileyemoticon)
            put("19", R.drawable.notimpressedemoticon)
            put("20", R.drawable.winkemoticon)
            put("21", R.drawable.cheekyemoticon)
            put("22", R.drawable.grinemoticon)
            put("23", R.drawable.mylipsaresealedemoticon)
            put("24", R.drawable.surprisedemoticon)


            put("25", R.drawable.lmaoemoticon)
            put("26", R.drawable.lolemoticon)
            put("27", R.drawable.lostinloveemoticon)
            put("28", R.drawable.mischievousemoticon)
            put("29", R.drawable.mutteringemoticon)
            put("30", R.drawable.raisedeyebrowsemoticon)
            put("31", R.drawable.shockedemoticon)
            put("32", R.drawable.slashedemoticon)
            put("33", R.drawable.yawnemoticon)

            put("34", R.drawable.coolemoticon)
            put("35", R.drawable.alienemoticon)
            put("36", R.drawable.devilemoticon)
            put("37", R.drawable.angelemoticon)
            put("38", R.drawable.explodeemoticon)
            put("39", R.drawable.heartsemoticon)

            put("41", R.drawable.bigheart)
            put("43", R.drawable.happy)
            put("44", R.drawable.confettihearts)
            put("45", R.drawable.heartsemoticon)
            put("46", R.drawable.hi)
            put("47", R.drawable.howareyou)

            put("48", R.drawable.fabulous)
            put("49", R.drawable.loveyouroutfit)
            put("50", R.drawable.rose)
            put("53", R.drawable.yourethebest)
            put("57", R.drawable.byecat)
            put("58", R.drawable.excitedcat)

            put("59", R.drawable.haha)
            put("63", R.drawable.hellocat)
            put("65", R.drawable.kissylips)
            put("66", R.drawable.lickycat)
            put("67", R.drawable.manykissylips)
            put("70", R.drawable.smilecat)
            put("71", R.drawable.sorryfrog)
            put("76", R.drawable.enjoy)
            put("83", R.drawable.missyou)
            put("84", R.drawable.overjoyed)
            put("87", R.drawable.wow)
            put("88", R.drawable.cheers)
            put("90", R.drawable.happybirthday)

            put("91", R.drawable.happyeaster)
            put("94", R.drawable.lol)
            put("95", R.drawable.noway)
            put("96", R.drawable.ok)
            put("98", R.drawable.rotflmao)
            put("102", R.drawable.welcome)
            put("103", R.drawable.youreawesome)
            put("104", R.drawable.aprilfools)
            put("105", R.drawable.brbknob)


            put("106", R.drawable.cartoonpuppy01)
            put("107", R.drawable.cartoonpuppy02)
            put("108", R.drawable.cartoonsleepypuppy01)
            put("111", R.drawable.easterbasket)
            put("113", R.drawable.ilovebvg)
            put("114", R.drawable.iloveyoworld)
            put("116", R.drawable.no)
            put("117", R.drawable.nuhuh)
            put("118", R.drawable.relieved)
            put("119", R.drawable.whoohoo)
            put("121", R.drawable.bebacklater)


            put("122", R.drawable.cartoonrabbit01)
            put("124", R.drawable.hey)
            put("125", R.drawable.holdinghands)
            put("129", R.drawable.laughfrog)
            put("130", R.drawable.piggy)
            put("133", R.drawable.thanksfrog)


            put("134", R.drawable.thankyounote)
            put("135", R.drawable.whatababefrog)
            put("136", R.drawable.youforgotmybirthday)
        }
    }

    const val TYPE_SUPREME = "SUPREME"
    const val TYPE_PLAYER = "PLAYER"

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.SOURCE)
    @StringDef(TYPE_SUPREME, TYPE_PLAYER)
    annotation class UserType(val type: String)

    val connectionListener: AppClient.AppClientConnectionListener = object : AppClient.AppClientConnectionListener {
        override fun onClientConnected() {
            connectionConnected.onNext("You are connected to the Supreme Cloud")
        }

        override fun onClientDisconnect() {
            connectionDisconnected.onNext("You have disconnected from the Supreme Cloud")
        }

        override fun onClientRetrying() {
            connectionRetry.onNext("Reconnecting to the Supreme Cloud....")
        }
    }

    val connectSwf: PublishSubject<String> = PublishSubject.create()
    val connectionConnected: PublishSubject<String> = PublishSubject.create()
    val connectionRetry: PublishSubject<String> = PublishSubject.create()
    val connectionDisconnected: PublishSubject<String> = PublishSubject.create()
    val buddyUpdated: PublishSubject<BigBrotherResponse<BuddyWrapper, String>> = PublishSubject.create()
    val characterEntered: PublishSubject<BigBrotherResponse<RawCharacter, String>> = PublishSubject.create()
    val characterLeft: PublishSubject<BigBrotherResponse<CharacterLeft, String>> = PublishSubject.create()
    val room: PublishSubject<BigBrotherResponse<Room, String>> = PublishSubject.create()
    val events: PublishSubject<BigBrotherResponse<ArrayList<Event>, String>> = PublishSubject.create()
    val message: PublishSubject<BigBrotherResponse<Message, String>> = PublishSubject.create()

    val subject: PublishSubject<UserModel> = PublishSubject.create()
    val video: PublishSubject<ArrayList<YoutubeResult>> = PublishSubject.create()
    val music: PublishSubject<ArrayList<SoundCloudResult>> = PublishSubject.create()


    init {

    }
}
