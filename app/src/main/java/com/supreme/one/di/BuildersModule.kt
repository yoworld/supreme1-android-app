package com.supreme.one.di

import com.supreme.one.ui.main.tabs.account.AccountFragment
import com.supreme.one.ui.main.tabs.account.AccountModule
import com.supreme.one.ui.main.tabs.account.AccountViewModule
import com.supreme.one.ui.player.tabs.conversation.ConversationFragment
import com.supreme.one.ui.player.tabs.conversation.ConversationModule
import com.supreme.one.ui.player.tabs.conversation.ConversationViewModule
import com.supreme.one.ui.main.tabs.home.HomeFragment
import com.supreme.one.ui.main.tabs.home.HomeModule
import com.supreme.one.ui.main.tabs.home.HomeViewModule
import com.supreme.one.ui.login.LoginActivity
import com.supreme.one.ui.login.LoginModule
import com.supreme.one.ui.login.LoginViewModule
import com.supreme.one.ui.main.MainActivity
import com.supreme.one.ui.main.MainModule
import com.supreme.one.ui.main.MainViewModule
import com.supreme.one.ui.main.tabs.playlist.PlaylistFragment
import com.supreme.one.ui.main.tabs.playlist.PlaylistModule
import com.supreme.one.ui.main.tabs.playlist.PlaylistViewModule
import com.supreme.one.ui.main.tabs.settings.SettingFragment
import com.supreme.one.ui.main.tabs.settings.SettingModule
import com.supreme.one.ui.main.tabs.settings.SettingViewModule
import com.supreme.one.ui.main.tabs.soundcloud.SoundCloudSearchFragment
import com.supreme.one.ui.main.tabs.soundcloud.SoundCloudSearchModule
import com.supreme.one.ui.main.tabs.soundcloud.SoundCloudSearchViewModule
import com.supreme.one.ui.main.tabs.youtube.YoutubeSearchFragment
import com.supreme.one.ui.main.tabs.youtube.YoutubeSearchModule
import com.supreme.one.ui.main.tabs.youtube.YoutubeSearchViewModule
import com.supreme.one.ui.player.PlayerActivity
import com.supreme.one.ui.player.PlayerModule
import com.supreme.one.ui.player.PlayerViewModule
import com.supreme.one.ui.player.tabs.buddy.BuddyFragment
import com.supreme.one.ui.player.tabs.buddy.BuddyModule
import com.supreme.one.ui.player.tabs.buddy.BuddyViewModule
import com.supreme.one.ui.player.tabs.events.EventFragment
import com.supreme.one.ui.player.tabs.events.EventModule
import com.supreme.one.ui.player.tabs.events.EventViewModule
import com.supreme.one.ui.player.tabs.inbox.InboxFragment
import com.supreme.one.ui.player.tabs.inbox.InboxModule
import com.supreme.one.ui.player.tabs.inbox.InboxViewModule
import com.supreme.one.ui.player.tabs.room.RoomFragment
import com.supreme.one.ui.player.tabs.room.RoomModule
import com.supreme.one.ui.player.tabs.room.RoomViewModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Charlton on 1/30/18.
 */
@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = arrayOf(MainModule::class, MainViewModule::class))
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = arrayOf(HomeModule::class, HomeViewModule::class))
    internal abstract fun bindHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = arrayOf(AccountModule::class, AccountViewModule::class))
    internal abstract fun bindAccountFragment(): AccountFragment


    @ContributesAndroidInjector(modules = arrayOf(YoutubeSearchModule::class, YoutubeSearchViewModule::class))
    internal abstract fun bindYoutubeFragment(): YoutubeSearchFragment

    @ContributesAndroidInjector(modules = arrayOf(PlaylistModule::class, PlaylistViewModule::class))
    internal abstract fun bindPlaylistFragment(): PlaylistFragment

    @ContributesAndroidInjector(modules = arrayOf(LoginModule::class, LoginViewModule::class))
    internal abstract fun bindLoginActivity(): LoginActivity


    @ContributesAndroidInjector(modules = arrayOf(PlayerModule::class, PlayerViewModule::class))
    internal abstract fun bindPlayerActivity(): PlayerActivity

    @ContributesAndroidInjector(modules = arrayOf(ConversationModule::class, ConversationViewModule::class))
    internal abstract fun bindConversationFragment(): ConversationFragment

    @ContributesAndroidInjector(modules = arrayOf(RoomModule::class, RoomViewModule::class))
    internal abstract fun bindRoomFragment(): RoomFragment

    @ContributesAndroidInjector(modules = arrayOf(EventModule::class, EventViewModule::class))
    internal abstract fun bindEventFragment(): EventFragment

    @ContributesAndroidInjector(modules = arrayOf(BuddyModule::class, BuddyViewModule::class))
    internal abstract fun bindBuddyFragment(): BuddyFragment


    @ContributesAndroidInjector(modules = arrayOf(InboxModule::class, InboxViewModule::class))
    internal abstract fun bindInboxFragment(): InboxFragment


    @ContributesAndroidInjector(modules = arrayOf(SettingModule::class, SettingViewModule::class))
    internal abstract fun bindSettingFragment(): SettingFragment

    @ContributesAndroidInjector(modules = arrayOf(SoundCloudSearchModule::class, SoundCloudSearchViewModule::class))
    internal abstract fun bindSoundCloudSearchFragment(): SoundCloudSearchFragment

}
