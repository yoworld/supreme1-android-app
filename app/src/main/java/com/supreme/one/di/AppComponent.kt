package com.supreme.one.di

import com.supreme.one.App
import com.supreme.one.repository.user.UserRepositoryImpl
import com.supreme.one.ui.main.tabs.account.adapter.AccountRecyclerAdapter
import com.supreme.one.ui.player.tabs.conversation.ConversationPresenter
import com.supreme.one.ui.main.tabs.home.HomePresenter
import com.supreme.one.ui.login.LoginPresenter
import com.supreme.one.ui.main.MainPresenter
import com.supreme.one.ui.main.tabs.playlist.adapter.SoundCloudPlaylistRecyclerAdapter
import com.supreme.one.ui.main.tabs.playlist.adapter.YoutubePlaylistRecyclerAdapter
import com.supreme.one.ui.main.tabs.settings.SettingPresenter
import com.supreme.one.ui.main.tabs.soundcloud.adapter.SoundCloudSearchRecyclerAdapter
import com.supreme.one.ui.main.tabs.youtube.adapter.YoutubeSearchRecyclerAdapter
import com.supreme.one.ui.player.PlayerPresenter
import com.supreme.one.ui.player.tabs.buddy.BuddyPresenter
import com.supreme.one.ui.player.tabs.events.EventPresenter
import com.supreme.one.ui.player.tabs.inbox.InboxPresenter
import com.supreme.one.ui.player.tabs.room.RoomPresenter
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Charlton on 1/30/18.
 */

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class, BuildersModule::class))
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
    fun inject(presenter: LoginPresenter)
    fun inject(userRepositoryImpl: UserRepositoryImpl)
    fun inject(basePresenter: MainPresenter)
    fun inject(basePresenter: HomePresenter)
    fun inject(youtubeSearchRecyclerAdapter: YoutubeSearchRecyclerAdapter)
    fun inject(playlistRecyclerAdapter: YoutubePlaylistRecyclerAdapter)
    fun inject(accountRecyclerAdapter: AccountRecyclerAdapter)
    fun inject(conversationPresenter: ConversationPresenter)
    fun inject(playerPresenter: PlayerPresenter)
    fun inject(roomPresenter: RoomPresenter)
    fun inject(eventPresenter: EventPresenter)
    fun inject(buddyPresenter: BuddyPresenter)
    fun inject(inboxPresenter: InboxPresenter)
    fun inject(settingPresenter: SettingPresenter)
    fun inject(soundCloudSearchRecyclerAdapter: SoundCloudSearchRecyclerAdapter)
    fun inject(soundCloudPlaylistRecyclerAdapter: SoundCloudPlaylistRecyclerAdapter)


}
