package com.supreme.one.di

/**
 * Created by Charlton on 1/30/18.
 */


import android.content.Context
import android.os.Environment
import com.google.gson.GsonBuilder
import com.supreme.one.App
import com.supreme.one.Supreme
import com.supreme.one.network.*
import com.supreme.one.network.client.AppClient
import com.supreme.one.repository.buddy.BuddyRepository
import com.supreme.one.repository.buddy.BuddyRepositoryImpl
import com.supreme.one.repository.character.CharacterRepository
import com.supreme.one.repository.character.CharacterRepositoryImpl
import com.supreme.one.repository.soundcloud.SoundCloudRepository
import com.supreme.one.repository.soundcloud.SoundCloudRepositoryImpl
import com.supreme.one.repository.supreme.SupremeRepository
import com.supreme.one.repository.supreme.SupremeRepositoryImpl
import com.supreme.one.repository.user.UserRepository
import com.supreme.one.repository.user.UserRepositoryImpl
import com.supreme.one.repository.youtube.YoutubeRepository
import com.supreme.one.repository.youtube.YoutubeRepositoryImpl
import com.supreme.one.util.AuthInterceptor
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SharedPreferenceManagerImpl
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Charlton on 12/25/17.
 */

@Module
class AppModule(var application: App) {
    private val mUserServiceImpl: UserRepository
    private val mCharacterService: CharacterRepository
    private val mSupremeServiceImpl: SupremeRepository
    private val mYoutubeService: YoutubeRepository
    private val mBuddyService: BuddyRepository
    private val mSettingService: SettingService
    private val mSoundCloudService: SoundCloudRepository
    private val mSharedPreferenceManager: SharedPreferenceManager
    private var mAppClient: AppClient

    init {
        mSharedPreferenceManager = SharedPreferenceManagerImpl(application.getSharedPreferences("com.supreme.one", Context.MODE_PRIVATE))
        mAppClient = AppClient(application, mSharedPreferenceManager, "s1.supreme-one.net", 2002, true, Supreme.connectionListener)
        val httpLoggingInterceptor = HttpLoggingInterceptor( object: HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.v(message)
            }
        })
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val youtubeUserRetrofit = Retrofit.Builder()
                .baseUrl("http://s1.supreme-one.net:7411/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .setLenient()
                        .setDateFormat("yyyy-MM-dd HH:mm:ss")
                        .create()))
                .client(OkHttpClient.Builder()
                        .connectTimeout(300, TimeUnit.SECONDS)
                        .readTimeout(300, TimeUnit.SECONDS)
                        .writeTimeout(300, TimeUnit.SECONDS)
                        .cache(Cache(Environment.getDownloadCacheDirectory(), cacheSize))
                        .addInterceptor(AuthInterceptor(mSharedPreferenceManager, true))
                        .addInterceptor(httpLoggingInterceptor)
                        .build())
                .build()
        val userRetrofit = Retrofit.Builder()
                .baseUrl("http://s1.supreme-one.net:2052/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .setLenient()
                        .setDateFormat("yyyy-MM-dd HH:mm:ss")
                        .create()))
                .client(OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .cache(Cache(Environment.getDownloadCacheDirectory(), cacheSize))
                        .addInterceptor(AuthInterceptor(mSharedPreferenceManager, true))
                        .addInterceptor(httpLoggingInterceptor)
                        .build())
                .build()
        val accountRetrofit = Retrofit.Builder()
                .baseUrl("http://s1.supreme-one.net:2052/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .setLenient()
                        .setDateFormat("yyyy-MM-dd HH:mm:ss")
                        .create()))
                .client(OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .cache(Cache(Environment.getDownloadCacheDirectory(), cacheSize))
                        .addInterceptor(AuthInterceptor(mSharedPreferenceManager, false))
                        .addInterceptor(httpLoggingInterceptor)
                        .build())
                .build()
        mUserServiceImpl = UserRepositoryImpl(userRetrofit.create(UserService::class.java), mSharedPreferenceManager)
        mCharacterService = CharacterRepositoryImpl(accountRetrofit.create(CharacterService::class.java), mSharedPreferenceManager)
        mSupremeServiceImpl = SupremeRepositoryImpl(userRetrofit.create(SupremeService::class.java), mSharedPreferenceManager)
        mYoutubeService = YoutubeRepositoryImpl(youtubeUserRetrofit.create(YoutubeService::class.java), mSharedPreferenceManager)
        mSoundCloudService = SoundCloudRepositoryImpl(youtubeUserRetrofit.create(SoundCloudService::class.java), mSharedPreferenceManager)
        mSettingService = userRetrofit.create(SettingService::class.java)
        mBuddyService = BuddyRepositoryImpl(accountRetrofit.create(BuddyService::class.java), mSharedPreferenceManager)
    }


    @Provides
    @Singleton
    internal fun provideAppClient(): AppClient {
        if (mAppClient.isConnected() && mAppClient.isClosed()) {
            mAppClient = AppClient(application, mSharedPreferenceManager, "s1.supreme-one.net", 2002)
        }
        return mAppClient
    }

    @Provides
    @Singleton
    internal fun provideSPM(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }


    @Provides
    @Singleton
    internal fun provideCharacterService(): CharacterRepository {
        return mCharacterService
    }

    @Provides
    @Singleton
    internal fun provideUserService(): UserRepository {
        return mUserServiceImpl
    }

    @Provides
    @Singleton
    internal fun provideSupremeService(): SupremeRepository {
        return mSupremeServiceImpl
    }


    @Provides
    @Singleton
    internal fun providesYoutubeService(): YoutubeRepository {
        return mYoutubeService
    }

    @Provides
    @Singleton
    internal fun providesBuddyService(): BuddyRepository {
        return mBuddyService
    }

    @Provides
    @Singleton
    internal fun providesSettingService(): SettingService {
        return mSettingService
    }

    @Provides
    @Singleton
    internal fun providesSoundCloudService(): SoundCloudRepository {
        return mSoundCloudService
    }

    companion object {
        val cacheSize = (20 * 1024 * 1024).toLong()
    }

}