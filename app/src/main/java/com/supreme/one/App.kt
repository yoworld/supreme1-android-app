package com.supreme.one

import android.app.Activity
import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.supreme.one.di.AppComponent
import com.supreme.one.di.AppModule
import com.supreme.one.di.DaggerAppComponent
import com.supreme.one.util.FontOverride
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.fabric.sdk.android.Fabric
import io.realm.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Charlton on 1/30/18.
 */

class App : Application(), HasActivityInjector {
    @Inject
    lateinit var mDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        Realm.init(this)
        val config = RealmConfiguration.Builder()
                .name("supreme.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1).build()
        Realm.setDefaultConfiguration(config)
        Timber.plant(Timber.DebugTree())
        build = DaggerAppComponent.builder()
                .application(this)
                .appModule(AppModule(this))
                .build()
        build.inject(this)


        FontOverride.setDefaultFont(this, "DEFAULT", getString(R.string.fredoka_one))
        FontOverride.setDefaultFont(this, "MONOSPACE", getString(R.string.fredoka_one))

    }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return mDispatchingAndroidInjector
    }

    companion object {
        lateinit var build: AppComponent
    }
}
