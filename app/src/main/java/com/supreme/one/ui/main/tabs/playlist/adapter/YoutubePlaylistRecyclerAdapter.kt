package com.supreme.one.ui.main.tabs.playlist.adapter

import android.app.AlertDialog
import android.app.ProgressDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.supreme.one.App
import com.supreme.one.R
import com.supreme.one.network.YoResponse
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.YoutubePlaylistModel
import com.supreme.one.repository.youtube.YoutubeRepository
import com.supreme.one.repository.youtube.YoutubeSpecification
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.reactivex.Completable
import io.reactivex.ObservableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import io.realm.Case
import io.realm.OrderedRealmCollection
import io.realm.Realm
import io.realm.Sort
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * Created by Charlton on 2/8/18.
 */
class YoutubePlaylistRecyclerAdapter(data: OrderedRealmCollection<YoutubePlaylistModel>?, autoUpdate: Boolean) :
        SupremeRealmRecyclerViewAdapter<YoutubePlaylistModel, YoutubePlaylistRecyclerAdapter.BaseHolder>(data, autoUpdate), SearchRecyclerAction {

    @Inject
    lateinit var youtubeRepository: YoutubeRepository
    var youtubeSpecification: YoutubeSpecification = YoutubeSpecification()

    @Inject
    lateinit var mAppClient: AppClient

    init {
        App.build.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder {
        return MusicHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))

    }


    private fun exists(user_id: Int, youtube_id: String): Boolean {
        val query = youtubeRepository.query(youtubeSpecification, object : HashMap<String, Any>() {
            init {
                put("youtube_id", youtube_id)
                put("user_id", user_id)
            }
        })
        return query.isNotEmpty()
    }

    override fun onBindViewHolder(holder: BaseHolder, position: Int) {
        val item = getItem(position)!!
        (holder as MusicHolder).playlist_title.text = item.title
        holder.playlist_time.text = item.getYoutubeLink()
        holder.playlist_description.text = item.created_at!!.toLocaleString()
        Glide.with(holder.itemView.context).asBitmap().load(item.jpg_link).into(holder.playlist_thumb)


        holder.playlist_download.visibility = View.VISIBLE
        val user_id = youtubeRepository.getSharedPreferenceManager().getId()
        val youtube_id = item.youtube_id!!
        if (exists(user_id, youtube_id)) {
            val drawable = ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_006_close)
            holder.playlist_download.setImageDrawable(drawable)
        }
        holder.itemView.setOnClickListener {
            val video_id = item.youtube_id
            Completable.defer { Completable.fromAction { mAppClient.playMusic(video_id!!) } }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ Toast.makeText(holder.itemView.context, String.format("Playing: %s", item.title), Toast.LENGTH_SHORT).show() }, {
                        Timber.e(it)
                        Toast.makeText(holder.itemView.context, String.format("Unable to play: %s - %s", item.title, it.message), Toast.LENGTH_SHORT).show()
                    })
        }
        holder.playlist_download.setOnClickListener {
            val progress = ProgressDialog(holder.itemView.context).also {
                it.setTitle("Please Wait")
                it.setMessage("Verifying Song")
                it.setCancelable(false)
                it.setCanceledOnTouchOutside(false)
            }
            progress.show()
            if (exists(user_id, youtube_id)) {
                youtubeRepository.removeSong(user_id, youtube_id)
                        .flatMap(Function<YoResponse<String, String>, ObservableSource<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>>> {
                            return@Function youtubeRepository.getCurrentPlaylist()
                        }).subscribe({
                            progress.dismiss()
                            AlertDialog.Builder(holder.itemView.context).setTitle("Supreme Playlist Removed").setMessage(String.format("%s was removed from your playlist.")).setPositiveButton("OK") { dialog, which ->
                                dialog?.dismiss()
                            }.show()
                        }, {
                            progress.dismiss()
                            Timber.e(it, "ERROR: %s", it.message)
                        })
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_view_playlist
    }


    override fun search(query: String) {
        var where = Realm.getDefaultInstance().where(YoutubePlaylistModel::class.java)
        where = where.contains("title", query, Case.INSENSITIVE)
        where = where.sort("position", Sort.ASCENDING)
        updateData(where.findAll())
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    class MusicHolder(view: View) : BaseHolder(view) {
        var playlist_title: AppCompatTextView = view.findViewById(R.id.playlist_title)
        var playlist_thumb: AppCompatImageView = view.findViewById(R.id.playlist_audio)
        var playlist_time: AppCompatTextView = view.findViewById(R.id.playlist_time)
        var playlist_description: AppCompatTextView = view.findViewById(R.id.playlist_description)
        var playlist_download: AppCompatImageView = view.findViewById(R.id.view_download)
    }

    open class BaseHolder(view: View) : RecyclerView.ViewHolder(view)


}