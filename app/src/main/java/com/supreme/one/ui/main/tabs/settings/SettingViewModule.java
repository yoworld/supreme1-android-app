package com.supreme.one.ui.main.tabs.settings;


import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class SettingViewModule {

    @Binds
    abstract SettingContract.View provideFragmentSettingView(SettingFragment settingFragment);
}
