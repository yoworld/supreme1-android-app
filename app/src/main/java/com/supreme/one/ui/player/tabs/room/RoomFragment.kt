package com.supreme.one.ui.player.tabs.room

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.supreme.one.R
import com.supreme.one.Supreme
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.client.models.RawCharacter
import com.supreme.one.ui.player.tabs.room.adapter.RoomRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.realm.Realm
import io.realm.Sort
import timber.log.Timber
import javax.inject.Inject


class RoomFragment : BaseFragment(), RoomContract.View {

    @Inject
    lateinit var presenter: RoomContract.Presenter
    private lateinit var mRecyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_room, container, false)
        mRecyclerView = view.findViewById(R.id.room_recycler)
        mRecyclerView.addItemDecoration(GridSpacingItemDecoration(1, 10, false))
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView.adapter = RoomRecyclerAdapter(
                Realm.getDefaultInstance().where(RawCharacter::class.java)
                        .sort("name", Sort.ASCENDING)
                        .findAll(),
                sharedPreferenceManager
        )
        disposables.add(Supreme.characterEntered.observeOn(AndroidSchedulers.mainThread()).subscribe {
            (mRecyclerView.adapter as RoomRecyclerAdapter).updateData(
                    Realm.getDefaultInstance().where(RawCharacter::class.java)
                            .sort("name", Sort.ASCENDING)
                            .findAll()
            )
        })
        disposables.add(Supreme.characterLeft.observeOn(AndroidSchedulers.mainThread()).subscribe {})
        disposables.add(Supreme.room.observeOn(AndroidSchedulers.mainThread()).subscribe {
            Timber.w("HIT: Changed Room: %s", it.data.roomId)
            (mRecyclerView.adapter as RoomRecyclerAdapter).updateData(
                    Realm.getDefaultInstance().where(RawCharacter::class.java)
                            .sort("name", Sort.ASCENDING)
                            .findAllAsync()
            )
        })
    }

    override fun onError(throwable: Throwable) {
        Toast.makeText(context, "Error: " + throwable.message, Toast.LENGTH_LONG).show()
        Timber.e(throwable)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

}

