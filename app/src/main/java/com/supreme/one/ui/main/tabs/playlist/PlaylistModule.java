package com.supreme.one.ui.main.tabs.playlist;

import com.supreme.one.repository.soundcloud.SoundCloudRepository;
import com.supreme.one.repository.youtube.YoutubeRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class PlaylistModule {
    @Provides
    PlaylistContract.Presenter providesPlaylistPresenter(PlaylistContract.View view, YoutubeRepository youtubeRepository, SoundCloudRepository soundCloudRepository) {
        return new PlaylistPresenter(view, youtubeRepository, soundCloudRepository);
    }
}
