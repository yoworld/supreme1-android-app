package com.supreme.one.ui.player.tabs.buddy;

import com.supreme.one.repository.character.CharacterRepository;
import com.supreme.one.ui.player.tabs.events.EventContract;
import com.supreme.one.ui.player.tabs.events.EventPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class BuddyModule {
    @Provides
    BuddyContract.Presenter providesBuddyresenter(BuddyContract.View view, CharacterRepository repository) {
        return new BuddyPresenter(view, repository);
    }
}
