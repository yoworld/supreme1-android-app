package com.supreme.one.ui.main.tabs.settings;

import com.supreme.one.network.SettingService;
import com.supreme.one.repository.user.UserRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class SettingModule {
    @Provides
    SettingContract.Presenter providesSettingsPresenter(SettingContract.View view, UserRepository repository, SettingService settingService) {
        return new SettingPresenter(view, repository, settingService);
    }
}
