package com.supreme.one.ui.main.tabs.youtube;



import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class YoutubeSearchViewModule {

    @Binds
    abstract YoutubeSearchContract.View provideFragmentYoutubeSearchView(YoutubeSearchFragment playlistFragment);
}
