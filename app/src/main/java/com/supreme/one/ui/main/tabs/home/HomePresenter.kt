package com.supreme.one.ui.main.tabs.home

import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.user.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Charlton on 1/30/18.
 */

class HomePresenter(val view: HomeContract.View, val userService: UserRepository) : BasePresenter<HomeContract.View>(view), HomeContract.Presenter {


    init {
        App.build.inject(this)
    }


    override fun getAuthenticatedAccount() {
        userService.currentUser
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onAccountFetched(it.data!!) }, { view.onAccountFetchError(it) })
    }

}
