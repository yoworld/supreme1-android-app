package com.supreme.one.ui.main.tabs.account

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.models.SupremeAccount
import com.supreme.one.network.models.UserModel
import com.supreme.one.ui.main.tabs.account.adapter.AccountRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import io.realm.Realm
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Charlton on 2/7/18.
 */
class AccountFragment : BaseFragment(), AccountContract.View {

    lateinit var mRecyclerView: RecyclerView

    @Inject
    lateinit var presenter: AccountContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_account, container, false)
        mRecyclerView = view.findViewById(R.id.account_recycler)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView.addItemDecoration(GridSpacingItemDecoration(1,10,false))
        mRecyclerView.adapter = AccountRecyclerAdapter(Realm.getDefaultInstance().where(SupremeAccount::class.java).findAll())
    }


    override fun onUserLoggedIn(user: UserModel) {
        super.onUserLoggedIn(user)
        presenter.getAccounts(user.id!!)
        (mRecyclerView.adapter as AccountRecyclerAdapter)
                .updateData(Realm.getDefaultInstance().where(SupremeAccount::class.java).equalTo("assigned_to", user.id).findAll())
    }

    override fun onAccountFetched(user: List<SupremeAccount>) {

    }

    override fun onError(throwable: Throwable) {
        Timber.e(throwable)
    }

    override fun onAccountFetchError(throwable: Throwable) {
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }

}