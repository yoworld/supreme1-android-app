package com.supreme.one.ui.main

import com.supreme.one.App
import com.supreme.one.Supreme
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.UserService
import com.supreme.one.repository.user.UserRepository
import com.supreme.one.util.SharedPreferenceManager
import javax.inject.Inject


/**
 * Created by Charlton on 1/1/18.
 */
class MainPresenter(private var view: MainContract.View, private var credentialService: UserRepository) : BasePresenter<MainContract.View>(view), MainContract.Presenter {

    init {
        App.build.inject(this)
    }

    override fun getCurrentUser() {
        credentialService.currentUser.subscribe({
            sharedPreferenceManager.saveUser(it.data)
            sharedPreferenceManager.saveId(it.data!!.id!!)
        },{})
    }

}