package com.supreme.one.ui.player.tabs.buddy

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/30/18.
 */

interface BuddyContract {
    interface View : BaseView {
        fun onError(throwable: Throwable)
        fun onUpdating()
    }

    interface Presenter {
        fun refresh()
        fun stop()
    }
}

