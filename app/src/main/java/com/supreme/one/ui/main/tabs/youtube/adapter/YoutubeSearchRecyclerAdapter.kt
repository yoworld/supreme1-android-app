package com.supreme.one.ui.main.tabs.youtube.adapter

import android.app.AlertDialog
import android.app.ProgressDialog
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.supreme.one.App
import com.supreme.one.R
import com.supreme.one.network.YoResponse
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.YoutubePlaylistModel
import com.supreme.one.network.models.YoutubeResult
import com.supreme.one.repository.youtube.YoutubeRepository
import com.supreme.one.repository.youtube.YoutubeSpecification
import com.supreme.one.ui.main.tabs.playlist.adapter.YoutubePlaylistRecyclerAdapter
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.reactivex.Completable
import io.reactivex.ObservableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import io.realm.Case
import io.realm.OrderedRealmCollection
import io.realm.Realm
import okhttp3.ResponseBody
import timber.log.Timber
import java.util.*
import javax.inject.Inject


/**
 * Created by Charlton on 2/10/18.
 */

class YoutubeSearchRecyclerAdapter(data: OrderedRealmCollection<YoutubeResult>?)
    : SupremeRealmRecyclerViewAdapter<YoutubeResult, YoutubePlaylistRecyclerAdapter.MusicHolder>(data, true) {

    @Inject
    lateinit var youtubeRepository: YoutubeRepository

    @Inject
    lateinit var mAppClient: AppClient


    var youtubeSpecification: YoutubeSpecification = YoutubeSpecification()


    init {
        App.build.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YoutubePlaylistRecyclerAdapter.MusicHolder {
        return YoutubePlaylistRecyclerAdapter.MusicHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.holder_view_playlist, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_view_playlist
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    fun search(query: String) {
        var where = Realm.getDefaultInstance().where(YoutubeResult::class.java)
        where = where
                .contains("title", query, Case.INSENSITIVE).or()
                .contains("description", query, Case.INSENSITIVE).or()
                .contains("channel", query, Case.INSENSITIVE)
        updateData(where.findAll())
    }


    fun exists(user_id: Int, youtube_id: String): Boolean {
        val query = youtubeRepository.query(youtubeSpecification, object : HashMap<String, Any>() {
            init {
                put("youtube_id", youtube_id)
                put("user_id", user_id)
            }
        })
        return query.isNotEmpty()
    }

    /**
     * @see YoutubeRepository
     */
    override fun onBindViewHolder(holder: YoutubePlaylistRecyclerAdapter.MusicHolder, position: Int) {
        var item = getItem(position)!!
        holder.playlist_title.text = item.title
        holder.playlist_time.text = item.channel
        Glide.with(holder.playlist_thumb).asBitmap().load(item.thumbnail).transition(withCrossFade()).into(holder.playlist_thumb)
        holder.playlist_description.text = item.description!!
        holder.playlist_download.visibility = View.VISIBLE
        val user_id = youtubeRepository.getSharedPreferenceManager().getId()
        val youtube_id = item.video_id!!
        val title = item.title
        if (exists(user_id, youtube_id)) {
            val drawable = ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_006_close)
            holder.playlist_download.setImageDrawable(drawable)
        } else {
            val drawable = ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_103_download_1)
            holder.playlist_download.setImageDrawable(drawable)
        }
        holder.itemView.setOnClickListener {
            val video_id = item.video_id
            Completable.defer {
                Completable.fromAction {
                    mAppClient.playMusic(video_id!!)
                }
            }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ Toast.makeText(holder.itemView.context, String.format("Playing: %s", item.title), Toast.LENGTH_SHORT).show() }, {
                        Timber.e(it)
                        Toast.makeText(holder.itemView.context, String.format("Unable to play: %s - %s", item.title, it.message), Toast.LENGTH_SHORT).show()
                    })
        }
        holder.playlist_download.setOnClickListener {
            val progress = ProgressDialog(holder.itemView.context).also {
                it.setTitle("Please Wait")
                it.setMessage("Verifying Song")
                it.setCancelable(false)
                it.setCanceledOnTouchOutside(false)
            }
            progress.show()
            if (exists(user_id, youtube_id)) {
                youtubeRepository.removeSong(user_id, youtube_id)
                        .flatMap(Function<YoResponse<String, String>, ObservableSource<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>>> {
                            return@Function youtubeRepository.getCurrentPlaylist()
                        }).subscribe({
                            progress.dismiss()
                            AlertDialog.Builder(holder.itemView.context).setTitle("Supreme Playlist Removed").setMessage(String.format("%s was removed from your playlist.", title)).setPositiveButton("OK") { dialog, _ ->
                                dialog?.dismiss()
                            }.show()
                        }, {
                            progress.dismiss()
                            Timber.e(it, "ERROR: %s", it.message)
                        }, {
                            val drawable = ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_103_download_1)
                            holder.playlist_download.setImageDrawable(drawable)
                        })
            } else {
                youtubeRepository.getYoutubeSong(youtube_id)
                        .flatMap(Function<ResponseBody, ObservableSource<YoResponse<YoutubePlaylistModel, PaginationData>>> {
                            progress.setMessage("Adding Song")
                            return@Function youtubeRepository.addSong(user_id, object : HashMap<String, Any>() {
                                init {
                                    put("youtube_id", youtube_id)
                                    put("position", 0)
                                }
                            })

                        }).flatMap(Function<YoResponse<YoutubePlaylistModel, PaginationData>, ObservableSource<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>>> {
                    return@Function youtubeRepository.getCurrentPlaylist()
                }).subscribe({
                    progress.dismiss()
                    AlertDialog.Builder(holder.itemView.context).setTitle("Supreme Playlist Updated").setMessage(String.format("%s was added to your playlist", title)).setPositiveButton("OK") { dialog, _ ->
                        dialog?.dismiss()
                    }.show()
                }, {
                    progress.dismiss()
                    Timber.e(it, "ERROR: %s", it.message)
                }, {
                    val drawable = ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_006_close)
                    holder.playlist_download.setImageDrawable(drawable)
                })
            }
        }
    }
}
