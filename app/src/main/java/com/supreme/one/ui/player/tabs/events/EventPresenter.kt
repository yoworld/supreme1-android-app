package com.supreme.one.ui.player.tabs.events

import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.client.models.Room
import com.supreme.one.repository.character.CharacterRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Charlton on 1/30/18.
 */

class EventPresenter(val view: EventContract.View,
                     val characterRepository: CharacterRepository
) : BasePresenter<EventContract.View>(view), EventContract.Presenter {
    override fun changeEvent(category: Int) {
        addDisposable(Completable.defer {
            Completable.fromAction {
                val sendMessage = mAppClient.getCategory(category)
                if (!sendMessage) throw Exception("Unable to change event category")
            }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onChange() }, { view.onError(it) }))
    }

    override fun join(join: Room) {
        addDisposable(Completable.defer {
            Completable.fromAction {
                join.forPlayerId = sharedPreferenceManager.getAccountId()
                val sendMessage = mAppClient.join(join)
                if (!sendMessage) throw Exception("Unable to join room")
            }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onJoin() }, { view.onError(it) }))
    }

    init {
        App.build.inject(this)
    }


}
