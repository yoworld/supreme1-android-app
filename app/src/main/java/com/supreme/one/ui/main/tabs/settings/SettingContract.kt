package com.supreme.one.ui.main.tabs.settings

import com.supreme.one.base.BaseView
import com.supreme.one.network.models.YovilleAccountSettings

/**
 * Created by Charlton on 1/30/18.
 */

interface SettingContract {
    interface View : BaseView {
        fun onSettingsUpdated(message: String?)
        fun onSettingsFetched(settings: YovilleAccountSettings)
        fun onError(throwable: Throwable)
    }

    interface Presenter {
        fun getSettings()
        fun saveSettings(settings: YovilleAccountSettings)
        fun saveSettings(settings: HashMap<String, Any>)
        fun stop()
    }
}

