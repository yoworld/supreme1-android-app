package com.supreme.one.ui.player.tabs.conversation

import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.client.BigBrotherResponse
import com.supreme.one.network.client.models.SendMessage
import com.supreme.one.repository.character.CharacterRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Charlton on 1/30/18.
 */

class ConversationPresenter(val view: ConversationContract.View,
                            val characterRepository: CharacterRepository
) : BasePresenter<ConversationContract.View>(view), ConversationContract.Presenter {

    init {
        App.build.inject(this)
    }

    override fun sendMessage(message: String) {
        addDisposable(Completable.defer {
            Completable.fromAction {
                val sendMessage = mAppClient.sendMessage(message)
                if(!sendMessage) throw Exception("Unable to send message")
            }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onMessageSent() }, { view.onError(it) }))
    }


}
