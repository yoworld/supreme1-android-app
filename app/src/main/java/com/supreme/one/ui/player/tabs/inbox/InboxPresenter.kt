package com.supreme.one.ui.player.tabs.inbox

import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.character.CharacterRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Charlton on 1/30/18.
 */

class InboxPresenter(val view: InboxContract.View,
                     val characterRepository: CharacterRepository
) : BasePresenter<InboxContract.View>(view), InboxContract.Presenter {
    override fun sendMessage(to: Int, message: String) {
        addDisposable(Completable.defer {
            Completable.fromAction {
                val sendMessage = mAppClient.sendPrivateMessage(to,message)
                if(!sendMessage) throw Exception("Unable to send message")
            }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onMessageSent() }, { view.onError(it) }))

    }

    init {
        App.build.inject(this)
    }


}
