package com.supreme.one.ui.main;



import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/1/18.
 */

@Module
public abstract class MainViewModule {
    @Binds
    abstract MainContract.View provideMainView(MainActivity mainActivity);
}
