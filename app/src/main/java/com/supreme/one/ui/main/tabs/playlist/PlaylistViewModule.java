package com.supreme.one.ui.main.tabs.playlist;


import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class PlaylistViewModule {

    @Binds
    abstract PlaylistContract.View provideFragmentPlaylistView(PlaylistFragment playlistFragment);
}
