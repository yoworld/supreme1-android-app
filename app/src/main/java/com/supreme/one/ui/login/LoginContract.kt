package com.supreme.one.ui.login

import com.facebook.FacebookCallback
import com.facebook.login.LoginResult
import com.google.android.gms.common.api.GoogleApiClient
import com.supreme.one.base.BaseView
import com.supreme.one.network.models.UserModel
import com.supreme.one.network.request.CredentialModel

/**
 * Created by Charlton on 1/1/18.
 */
interface LoginContract {

    interface View : BaseView {
        fun onLoginStarted()
        fun onLoginSuccess(user: UserModel)
        fun onLoginCompleted()
        fun onLoginCanceled()
        fun onLoginFailed(throwable: Throwable)
    }

    interface Presenter: FacebookCallback<LoginResult>, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
        fun facebook(access_token: String)
        fun login(email: String, password: String)
        fun isLoggedIn(): Boolean
        fun stop()
    }

}