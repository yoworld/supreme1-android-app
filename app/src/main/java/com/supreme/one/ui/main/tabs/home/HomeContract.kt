package com.supreme.one.ui.main.tabs.home

import com.supreme.one.base.BaseView
import com.supreme.one.network.models.UserModel

/**
 * Created by Charlton on 1/30/18.
 */

interface HomeContract {
    interface View : BaseView {
        fun onAccountFetched(user: UserModel)

        fun onError(throwable: Throwable)

        fun onAccountFetchError(throwable: Throwable)
    }

    interface Presenter {
        fun getAuthenticatedAccount()
        fun stop()
    }
}

