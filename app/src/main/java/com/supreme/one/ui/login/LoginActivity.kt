package com.supreme.one.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import android.widget.Toast
import com.supreme.one.R
import com.supreme.one.base.BaseActivity
import com.supreme.one.network.models.UserModel
import com.supreme.one.ui.main.MainActivity
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract.View {

    @Inject
    lateinit var presenter: LoginContract.Presenter
    lateinit var mLoginBtn: AppCompatButton
    lateinit var mLoginEmail: AppCompatEditText
    lateinit var mLoginPassword: AppCompatEditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (presenter.isLoggedIn()) {
            onLoginCompleted()
        }
        setContentView(R.layout.activity_login)
        mLoginBtn = findViewById(R.id.login_btn)
        mLoginEmail = findViewById(R.id.login_email)
        mLoginPassword = findViewById(R.id.login_password)
        mLoginBtn.setOnClickListener { presenter.login(mLoginEmail.text.toString(), mLoginPassword.text.toString()) }
    }

    override fun onLoginStarted() {


    }

    override fun onLoginSuccess(user: UserModel) {
        Toast.makeText(this, String.format("Welcome %s", user.name), Toast.LENGTH_LONG).show()
    }

    override fun onLoginCompleted() {
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
        finish()
    }

    override fun onLoginCanceled() {

    }

    override fun onLoginFailed(throwable: Throwable) {
        Toast.makeText(this, "Invalid Email Or Password", Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }
}
