package com.supreme.one.ui.player.tabs.conversation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.RecyclerView
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.client.models.Message
import com.supreme.one.ui.player.tabs.conversation.adapter.ConversationRecyclerAdapter
import dagger.android.support.AndroidSupportInjection
import io.realm.Realm
import io.realm.Sort
import timber.log.Timber
import javax.inject.Inject


class ConversationFragment : BaseFragment(), ConversationContract.View {


    @Inject
    lateinit var presenter: ConversationContract.Presenter
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mEditText: AppCompatEditText
    private var mAdapterObserver: RecyclerView.AdapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            val item = (mRecyclerView.adapter as ConversationRecyclerAdapter).getItem(mRecyclerView.adapter!!.itemCount - 1)!!

            val textColor = item.getContrastColor(item.getColorHexInt())
            val colorStateList =  item.getColorHexInt() //ContextCompat.getColorStateList(holder.itemView.context, R.color.grey)


            scrollToLatestMessage(mRecyclerView, String.format("%s - %s", item.fromPlayerName, item.filteredMessage(context)), color = textColor, bg = colorStateList)
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            super.onItemRangeRemoved(positionStart, itemCount)
            val item = (mRecyclerView.adapter as ConversationRecyclerAdapter).getItem(mRecyclerView.adapter!!.itemCount - 1)!!
            val textColor = item.getContrastColor(item.getColorHexInt())
            val colorStateList =  item.getColorHexInt() //ContextCompat.getColorStateList(holder.itemView.context, R.color.grey)
            scrollToLatestMessage(mRecyclerView, String.format("%s - %s", item.fromPlayerName, item.filteredMessage(context)), color = textColor, bg = colorStateList)
        }
    }


    override fun onMessageSent() {
        Timber.i("Message Sent")
        mEditText.setText("")
    }

    override fun onError(throwable: Throwable) {
        Toast.makeText(context, "Error: " + throwable.message, Toast.LENGTH_LONG).show()
        Timber.e(throwable)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_conversation, container, false)
        mRecyclerView = view.findViewById(R.id.conversation_recycler)
        mEditText = view.findViewById(R.id.conversation_field)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView.adapter = ConversationRecyclerAdapter(
                Realm.getDefaultInstance().where(Message::class.java)
                        .notEqualTo("chatType", Message.PRIVATE)
                        .sort("createdAt", Sort.ASCENDING)
                        .findAllAsync(),
                sharedPreferenceManager
        )
        (mRecyclerView.adapter as ConversationRecyclerAdapter?)?.registerAdapterDataObserver(mAdapterObserver)
        mEditText.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                presenter.sendMessage(v.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    override fun onDestroyView() {
        presenter.stop()
        mRecyclerView.adapter?.unregisterAdapterDataObserver(mAdapterObserver)
        super.onDestroyView()
    }


}

