package com.supreme.one.ui.player.tabs.room

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/30/18.
 */

interface RoomContract {
    interface View : BaseView {
        fun onError(throwable: Throwable)
    }

    interface Presenter {
        fun stop()
    }
}

