package com.supreme.one.ui.player.tabs.inbox;

import com.supreme.one.repository.character.CharacterRepository;
import com.supreme.one.ui.player.tabs.buddy.BuddyContract;
import com.supreme.one.ui.player.tabs.buddy.BuddyPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class InboxModule {
    @Provides
    InboxContract.Presenter providesInboxPresenter(InboxContract.View view, CharacterRepository repository) {
        return new InboxPresenter(view, repository);
    }
}
