package com.supreme.one.ui.player.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.supreme.one.ui.player.tabs.buddy.BuddyFragment
import com.supreme.one.ui.player.tabs.conversation.ConversationFragment
import com.supreme.one.ui.player.tabs.events.EventFragment
import com.supreme.one.ui.player.tabs.inbox.InboxFragment
import com.supreme.one.ui.player.tabs.room.RoomFragment

/**
 * Created by Charlton on 3/2/18.
 */
class PlayerTabViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val mFragments: Array<Fragment> = arrayOf(
            BuddyFragment(),
            InboxFragment(),
            ConversationFragment(),
            EventFragment(),
            RoomFragment()
    )
    override fun getItem(position: Int): Fragment {
        return mFragments[position]
    }

    override fun getCount(): Int {
        return mFragments.size
    }
}