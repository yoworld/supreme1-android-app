package com.supreme.one.ui.main.tabs.playlist.adapter

interface SearchRecyclerAction {
    fun search(query: String)
}