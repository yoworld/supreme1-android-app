package com.supreme.one.ui.main.tabs.soundcloud

import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.soundcloud.SoundCloudRepository

/**
 * Created by Charlton on 1/30/18.
 */

class SoundCloudSearchPresenter(val view: SoundCloudSearchContract.View,
                                val supremeRepository: SoundCloudRepository
) : BasePresenter<SoundCloudSearchContract.View>(view), SoundCloudSearchContract.Presenter {

    override fun findSongs(query: String, page: Int, per: Int) {
        supremeRepository.findSongs(query, page, per).subscribe({ }, { view.onError(it) })
    }

    override fun addSong(user_id: Int, track_id: String) {
        supremeRepository.addSong(user_id, object : HashMap<String, Any>() {
            init {
                put("track_id", track_id)
            }
        }).subscribe({}, { view.onError(it) })
    }

}
