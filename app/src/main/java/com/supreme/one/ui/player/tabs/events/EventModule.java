package com.supreme.one.ui.player.tabs.events;

import com.supreme.one.repository.character.CharacterRepository;
import com.supreme.one.ui.player.tabs.room.RoomContract;
import com.supreme.one.ui.player.tabs.room.RoomPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class EventModule {
    @Provides
    EventContract.Presenter providesRoomPresenter(EventContract.View view, CharacterRepository repository) {
        return new EventPresenter(view, repository);
    }
}
