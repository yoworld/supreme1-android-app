package com.supreme.one.ui.main.tabs.soundcloud;

import com.supreme.one.repository.soundcloud.SoundCloudRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class SoundCloudSearchModule {
    @Provides
    SoundCloudSearchContract.Presenter providesPlaylistPresenter(SoundCloudSearchContract.View view, SoundCloudRepository repository) {
        return new SoundCloudSearchPresenter(view, repository);
    }
}
