package com.supreme.one.ui.player;



import com.supreme.one.ui.main.MainActivity;
import com.supreme.one.ui.main.MainContract;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/1/18.
 */

@Module
public abstract class PlayerViewModule {
    @Binds
    abstract PlayerContract.View providePlayerView(PlayerActivity mainActivity);
}
