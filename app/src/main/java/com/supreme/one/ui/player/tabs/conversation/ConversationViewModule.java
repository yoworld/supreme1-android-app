package com.supreme.one.ui.player.tabs.conversation;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class ConversationViewModule {

    @Binds
    abstract ConversationContract.View provideFragmentConversationView(ConversationFragment conversationFragment);
}
