package com.supreme.one.ui.player.tabs.buddy

import android.graphics.Color
import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.client.BigBrotherResponse
import com.supreme.one.network.client.models.SendMessage
import com.supreme.one.repository.character.CharacterRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Charlton on 1/30/18.
 */

class BuddyPresenter(val view: BuddyContract.View,
                     val characterRepository: CharacterRepository
) : BasePresenter<BuddyContract.View>(view), BuddyContract.Presenter {


    override fun refresh() {
        mAppClient.refreshBuddyList()
        addDisposable(Completable.defer {
            Completable.fromAction {
                val sendMessage = mAppClient.refreshBuddyList()
                if(!sendMessage) throw Exception("Unable to send message")
            }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onUpdating() }, { view.onError(it) }))
    }

    init {
        App.build.inject(this)
    }


}
