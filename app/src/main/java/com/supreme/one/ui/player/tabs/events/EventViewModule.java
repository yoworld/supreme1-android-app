package com.supreme.one.ui.player.tabs.events;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class EventViewModule {

    @Binds
    abstract EventContract.View provideFragmentEventView(EventFragment roomFragment);
}
