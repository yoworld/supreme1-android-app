package com.supreme.one.ui.main

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/1/18.
 */
interface MainContract {

    interface View : BaseView

    interface Presenter  {
        fun stop()
        fun getCurrentUser()
    }

}