package com.supreme.one.ui.player.tabs.events.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.supreme.one.R
import com.supreme.one.network.client.models.Event
import com.supreme.one.ui.player.tabs.events.EventContract
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.realm.Realm

/**
 * Created by Charlton on 3/3/18.
 */
class EventRecyclerAdapter(preferenceManager: SharedPreferenceManager, private val presenter: EventContract.Presenter) : SupremeRealmRecyclerViewAdapter<Event, EventRecyclerAdapter.EventHolder>(
        Realm.getDefaultInstance().where(Event::class.java)
                .equalTo("category", 0.toInt())
                .greaterThan("expires", System.currentTimeMillis())
                .findAllAsync(),
        true
) {


    var headers = arrayOf("All Ages Events", "Trading and Auctions", "Parties and Discussions", "Room Showcase", "Making Friends", "Theatre and Roleplay", "Dating - 18+", "Games")


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventHolder {
        return EventHolder(LayoutInflater.from(parent.context!!).inflate(viewType, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_event_view
    }

    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        val item = getItem(position)!!
        holder.mEventTitle.text = item.title
        holder.mEventDesc.text = item.desc
        holder.mEventPlayerName.text = item.createdBy
        holder.mEventTime.text = item.created
        Glide.with(holder.itemView.context).load(item.getPlayerAvatarUrl()).into(holder.mEventPlayerAvatar)
        holder.itemView.setOnClickListener {
            presenter.join(item.join())
        }
    }

    class EventHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mEventPlayerAvatar: AppCompatImageView = view.findViewById(R.id.event_player_avatar)
        var mEventPlayerName: AppCompatTextView = view.findViewById(R.id.event_player_name)
        var mEventTitle: AppCompatTextView = view.findViewById(R.id.event_text)
        var mEventDesc: AppCompatTextView = view.findViewById(R.id.event_desc)
        var mEventTime: AppCompatTextView = view.findViewById(R.id.event_time)
    }

    fun update(position: Int) {
        updateData(Realm.getDefaultInstance().where(Event::class.java)
                .equalTo("category", position)
                .greaterThan("expires", System.currentTimeMillis())
                .findAllAsync())
    }

}