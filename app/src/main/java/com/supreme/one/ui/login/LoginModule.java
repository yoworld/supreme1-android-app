package com.supreme.one.ui.login;


import com.supreme.one.repository.user.UserRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/1/18.
 */

@Module
public class LoginModule {
    @Provides
    LoginContract.Presenter providesLoginPresenter(LoginContract.View view, UserRepository service) {
        return new LoginPresenter(view, service);
    }
}
