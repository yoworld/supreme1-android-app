package com.supreme.one.ui.main.tabs.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.models.UserModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by Charlton on 2/7/18.
 */
class HomeFragment : BaseFragment(), HomeContract.View {

    @Inject
    lateinit var presenter: HomeContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }


    override fun onAccountFetchError(throwable: Throwable) {

    }

    override fun onError(throwable: Throwable) {

    }

    override fun onAccountFetched(user: UserModel) {

    }

    override fun onUserLoggedIn(user: UserModel) {
        super.onUserLoggedIn(user)
    }


}