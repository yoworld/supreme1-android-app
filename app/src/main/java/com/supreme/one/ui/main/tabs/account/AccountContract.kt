package com.supreme.one.ui.main.tabs.account

import com.supreme.one.base.BaseView
import com.supreme.one.network.models.SupremeAccount

/**
 * Created by Charlton on 1/30/18.
 */

interface AccountContract {
    interface View : BaseView {
        fun onAccountFetched(user: List<SupremeAccount>)

        fun onError(throwable: Throwable)

        fun onAccountFetchError(throwable: Throwable)
    }

    interface Presenter {
        fun getAccounts(user_id: Int)
        fun stop()
    }
}

