package com.supreme.one.ui.player

import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.UserService
import com.supreme.one.repository.user.UserRepository
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Charlton on 1/1/18.
 */
class PlayerPresenter(private var view: PlayerContract.View, private var credentialService: UserRepository) : BasePresenter<PlayerContract.View>(view), PlayerContract.Presenter {
    override fun getBuddies() {
        addDisposable(Completable.defer {
            Completable.fromAction {
                val sendMessage = mAppClient.getBuddies()
                if (!sendMessage) throw Exception("Unable to get buddies")
            }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Completed Task") }, { Timber.e(it.message) }))
    }

    init {
        App.build.inject(this)
    }

    override fun getCurrentUser() {
        credentialService.currentUser.subscribe({
            sharedPreferenceManager.saveUser(it.data)
            sharedPreferenceManager.saveId(it.data!!.id!!)
        },{})
    }

}