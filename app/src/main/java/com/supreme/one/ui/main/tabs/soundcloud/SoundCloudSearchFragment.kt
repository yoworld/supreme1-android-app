package com.supreme.one.ui.main.tabs.soundcloud

import android.content.Context
import android.os.Bundle
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.supreme.one.R
import com.supreme.one.Supreme
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.models.SoundCloudResult
import com.supreme.one.network.models.YoutubeResult
import com.supreme.one.ui.main.tabs.soundcloud.adapter.SoundCloudSearchRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import io.realm.Realm
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Charlton on 2/7/18.
 */
class SoundCloudSearchFragment : BaseFragment(), SoundCloudSearchContract.View {


    @Inject
    lateinit var presenter: SoundCloudSearchContract.Presenter
    lateinit var mSearchRecyclerView: RecyclerView
    lateinit var mSearchEditText: AppCompatEditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_playlists, container, false)
        mSearchRecyclerView = view.findViewById(R.id.playlist_recycler)
        mSearchEditText = view.findViewById(R.id.holder_search_et)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSearchRecyclerView.addItemDecoration(GridSpacingItemDecoration(1, 10, false))
        mSearchRecyclerView.adapter = SoundCloudSearchRecyclerAdapter(Realm.getDefaultInstance().where(SoundCloudResult::class.java).findAll())
        mSearchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (mSearchRecyclerView.adapter as SoundCloudSearchRecyclerAdapter).search(s.toString())
            }
        })
        mSearchEditText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Timber.e("SEARCHING %s",v.text.toString())
                presenter.findSongs(v.text.toString())
            }
            return@setOnEditorActionListener false
        }
        disposables.add(Supreme.music.subscribe { onSearchSongs(it) })
    }


    private fun onSearchSongs(results: List<SoundCloudResult>) {

    }

    override fun onError(throwable: Throwable) {

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }

}