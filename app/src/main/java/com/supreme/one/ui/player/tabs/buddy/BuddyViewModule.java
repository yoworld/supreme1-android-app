package com.supreme.one.ui.player.tabs.buddy;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class BuddyViewModule {

    @Binds
    abstract BuddyContract.View provideFragmenBuddyView(BuddyFragment roomFragment);
}
