package com.supreme.one.ui.main.tabs.settings

import android.annotation.SuppressLint
import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.SettingService
import com.supreme.one.network.models.YovilleAccountSettings
import com.supreme.one.repository.user.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Charlton on 1/30/18.
 */

class SettingPresenter(val view: SettingContract.View,
                       val userRepository: UserRepository,
                       val settingService: SettingService
) : BasePresenter<SettingContract.View>(view), SettingContract.Presenter {
    override fun saveSettings(settings: YovilleAccountSettings) {
        val user = userRepository.getSharedPreferenceManager().getUser()
        user?.user_settings?.id?.let { it ->
            settingService.settings(it, settings).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.onSettingsFetched(it.data!!)
                    view.onSettingsUpdated(it.message)
                }, { view.onError(it) })
        }

    }

    override fun saveSettings(settings: HashMap<String, Any>) {
        val user = userRepository.getSharedPreferenceManager().getUser()
        user?.user_settings?.id?.let { it ->
            settingService.settings(it, settings).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        view.onSettingsFetched(it.data!!)
                        view.onSettingsUpdated(it.message) }, { view.onError(it) })
        }
    }


    @SuppressLint("CheckResult")
    override fun getSettings() {
        userRepository.currentUser.subscribe({ it.data?.user_settings?.let { setting -> view.onSettingsFetched(setting) } }, { view.onError(it) })
    }


    init {
        App.build.inject(this)
    }


}
