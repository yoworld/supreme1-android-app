package com.supreme.one.ui.player.tabs.buddy.adapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.supreme.one.R
import com.supreme.one.network.client.models.Buddy
import com.supreme.one.network.client.models.RawCharacter
import com.supreme.one.ui.player.tabs.room.adapter.RoomRecyclerAdapter
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

/**
 * Created by Charlton on 3/3/18.
 */
class BuddyRecyclerAdapter(data: OrderedRealmCollection<Buddy>?, var preferenceManager: SharedPreferenceManager? = null) : SupremeRealmRecyclerViewAdapter<Buddy, RoomRecyclerAdapter.CharacterHolder>(data, true)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomRecyclerAdapter.CharacterHolder {
        return RoomRecyclerAdapter.CharacterHolder(LayoutInflater.from(parent.context!!).inflate(viewType, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_character_view
    }

    override fun onBindViewHolder(holder: RoomRecyclerAdapter.CharacterHolder, position: Int) {
        val item = getItem(position)!!
        holder.characterName.text = item.name
        holder.roomName.text = String.format("Status: (%s), IsOnline: (%s)", item.lastLogin!!, item.isOnline)
        Glide.with(holder.itemView.context).load(item.getAvatar()).into(holder.characterAvatar)
    }

}