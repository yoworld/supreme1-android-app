package com.supreme.one.ui.main.tabs.account;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class AccountViewModule {

    @Binds
    abstract AccountContract.View provideFragmentAccountView(AccountFragment accountFragment);
}
