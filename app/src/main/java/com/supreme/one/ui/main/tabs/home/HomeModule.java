package com.supreme.one.ui.main.tabs.home;

import com.supreme.one.repository.user.UserRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class HomeModule {
    @Provides
    HomeContract.Presenter providesHomePresenter(HomeContract.View view, UserRepository userService) {
        return new HomePresenter(view, userService);
    }
}
