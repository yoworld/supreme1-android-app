package com.supreme.one.ui.main.tabs.playlist

import android.content.Context
import android.os.Bundle
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.models.SoundCloudPlaylistModel
import com.supreme.one.network.models.YoutubePlaylistModel
import com.supreme.one.ui.main.tabs.playlist.adapter.SearchRecyclerAction
import com.supreme.one.ui.main.tabs.playlist.adapter.SoundCloudPlaylistRecyclerAdapter
import com.supreme.one.ui.main.tabs.playlist.adapter.YoutubePlaylistRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

/**
 * Created by Charlton on 2/7/18.
 */
class PlaylistFragment : BaseFragment(), PlaylistContract.View {

    @Inject
    lateinit var presenter: PlaylistContract.Presenter
    lateinit var mYoutubePlaylistRecyclerAdapter: YoutubePlaylistRecyclerAdapter
    lateinit var mSoundCloudPlaylistRecyclerAdapter: SoundCloudPlaylistRecyclerAdapter
    lateinit var mSearchEditText: AppCompatEditText
    lateinit var mPlaylistRecyclerView: RecyclerView
    lateinit var mPlay: AppCompatImageView
    lateinit var mStop: AppCompatImageView


    @Inject
    lateinit var mAppClient: AppClient



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_playlists, container, false)
        mPlaylistRecyclerView = view.findViewById(R.id.playlist_recycler)
        mSearchEditText = view.findViewById(R.id.holder_search_et)
        mPlay = view.findViewById(R.id.playlist_play)
        mStop = view.findViewById(R.id.playlist_stop)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPlay.setOnClickListener {
            disposables.add(Completable.defer {
                Completable.fromAction {
                    val sendMessage = mAppClient.sendMessage("<playlist>")
                    if(!sendMessage) throw Exception("Unable to play music from playlist")
                }
            }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ }, { onError(it) }))
        }
        mStop.setOnClickListener {
            disposables.add(Completable.defer {
                Completable.fromAction {
                    val sendMessage = mAppClient.sendMessage("<stop>")
                    if(!sendMessage) throw Exception("Unable to stop music")
                }
            }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({  }, { onError(it) }))
        }
        mPlaylistRecyclerView.addItemDecoration(GridSpacingItemDecoration(1, 10, false))
        this.mYoutubePlaylistRecyclerAdapter = YoutubePlaylistRecyclerAdapter(
                Realm.getDefaultInstance().where(YoutubePlaylistModel::class.java)
                        .sort("position", Sort.ASCENDING)
                        .findAll()
                , true)
        this.mSoundCloudPlaylistRecyclerAdapter = SoundCloudPlaylistRecyclerAdapter(
                Realm.getDefaultInstance().where(SoundCloudPlaylistModel::class.java)
                        .sort("position", Sort.ASCENDING)
                        .findAll()
                , true)
        mPlaylistRecyclerView.adapter = mSoundCloudPlaylistRecyclerAdapter
        mSearchEditText.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int){}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (mPlaylistRecyclerView.adapter as SearchRecyclerAction).search(s.toString())
            }
        })
        presenter.getSoundCloudPlaylist(sharedPreferenceManager.getId())
    }

    override fun onYoutubePlaylistFetched(user: List<YoutubePlaylistModel>) {

    }

    override fun onSoundCloudPlaylistFetched(user: List<SoundCloudPlaylistModel>) {

    }

    override fun onError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }

}