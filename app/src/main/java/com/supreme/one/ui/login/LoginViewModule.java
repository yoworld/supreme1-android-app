package com.supreme.one.ui.login;


import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/1/18.
 */

@Module
public abstract class LoginViewModule {
    @Binds
    abstract LoginContract.View provideLoginView(LoginActivity loginActivity);
}
