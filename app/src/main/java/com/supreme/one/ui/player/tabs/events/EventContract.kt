package com.supreme.one.ui.player.tabs.events

import com.supreme.one.base.BaseView
import com.supreme.one.network.client.models.Room

/**
 * Created by Charlton on 1/30/18.
 */

interface EventContract {
    interface View : BaseView {
        fun onError(throwable: Throwable)
        fun onJoin()
        fun onChange()
    }

    interface Presenter {
        fun join(join: Room)
        fun changeEvent(category: Int)
        fun stop()
    }
}

