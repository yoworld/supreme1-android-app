package com.supreme.one.ui.main.tabs.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.google.gson.Gson
import com.supreme.one.R
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.models.YovilleAccountSettings
import com.supreme.one.util.SharedPreferenceManager
import dagger.android.support.AndroidSupportInjection
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Charlton on 2/7/18.
 */
class SettingFragment : PreferenceFragmentCompat(), SettingContract.View, SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onSettingsFetched(settings: YovilleAccountSettings) {
        val settings = JSONObject(Gson().toJson(settings))
        val editor = preferenceManager.sharedPreferences.edit()
        settings.keys().forEach {
            val value = settings.get(it)
            when (value) {
                is String -> editor.putString(it, value.toString())
                is Int -> editor.putInt(it, value.toInt())
                is Boolean -> editor.putBoolean(it, value)
            }
        }
        editor.apply()
        Timber.e("Settings Fetched")
    }

    override fun onSettingsUpdated(settings: String?) {
        Timber.e(settings)
    }

    override fun onError(throwable: Throwable) {
        Timber.e(throwable)
    }

    @Inject
    lateinit var presenter: SettingContract.Presenter


    @Inject
    lateinit var mSharedPreferenceManager: SharedPreferenceManager

    @Inject
    lateinit var mAppClient: AppClient

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

        //registerOnSharedPreferenceChangeListener(mSharedPreferenceListener);
        preferenceManager.sharedPreferencesMode = Context.MODE_PRIVATE
        preferenceManager.sharedPreferencesName = "com.supreme.one.settings"
        setPreferencesFromResource(R.xml.preferences, rootKey)
        Timber.e(preferenceManager.sharedPreferences.all.toString())
        presenter.getSettings()

        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onDestroyView() {
        presenter.stop()
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
        super.onDestroyView()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        val value = sharedPreferences?.all?.get(key)
        Timber.e("KEY: %s - Value: %s", key, value)
        presenter.saveSettings(object : HashMap<String, Any>() {
            init {
                if (value != null && key != null) {
                    put(key, value)
                }
            }
        })
    }

}