package com.supreme.one.ui.main.tabs.youtube;

import com.supreme.one.repository.youtube.YoutubeRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class YoutubeSearchModule {
    @Provides
    YoutubeSearchContract.Presenter providesPlaylistPresenter(YoutubeSearchContract.View view, YoutubeRepository repository) {
        return new YoutubeSearchPresenter(view, repository);
    }
}
