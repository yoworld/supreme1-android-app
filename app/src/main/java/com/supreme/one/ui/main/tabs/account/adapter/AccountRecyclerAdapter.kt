package com.supreme.one.ui.main.tabs.account.adapter

import android.content.Intent
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.supreme.one.App
import com.supreme.one.R
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.models.SupremeAccount
import com.supreme.one.ui.player.PlayerActivity
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import javax.inject.Inject

/**
 * Created by Charlton on 2/8/18.
 */
class AccountRecyclerAdapter(data: OrderedRealmCollection<SupremeAccount>?) : SupremeRealmRecyclerViewAdapter<SupremeAccount, AccountRecyclerAdapter.BaseHolder>(data, true) {


    @Inject
    lateinit var mAppClient: AppClient

    @Inject
    lateinit var mSharedPreferenceManager: SharedPreferenceManager

    init {
        App.build.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder {
        return AccountHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
    }

    override fun onBindViewHolder(holder: BaseHolder, position: Int) {
        (holder as AccountHolder).account_name.text = getItem(position)!!.user_name
        holder.account_fb.text = getItem(position)!!.user_facebook_name
        Glide.with(holder.itemView.context).asBitmap().load(getItem(position)!!.user_avatar).into(holder.account_avatar)
        holder.itemView.setOnClickListener {
            if (getItem(position)!!.api_token != null) {
                mSharedPreferenceManager.setAccountId(getItem(position)!!.user_id)
                mSharedPreferenceManager.setAccountToken(getItem(position)!!.api_token!!)
                holder.itemView.context.startActivity(Intent(holder.itemView.context, PlayerActivity::class.java))
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_view_accounts
    }


    class AccountHolder(view: View) : BaseHolder(view) {
        var account_name: AppCompatTextView = view.findViewById(R.id.account_name)
        var account_avatar: AppCompatImageView = view.findViewById(R.id.account_avatar)
        var account_fb: AppCompatTextView = view.findViewById(R.id.account_fb)

    }

    open class BaseHolder(view: View) : RecyclerView.ViewHolder(view)
}