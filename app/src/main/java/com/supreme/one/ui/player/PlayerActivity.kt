package com.supreme.one.ui.player

import android.os.Bundle
import android.view.MenuItem
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.supreme.one.R
import com.supreme.one.base.BaseActivity
import com.supreme.one.ui.player.adapter.PlayerTabViewPagerAdapter
import com.supreme.one.util.SupremeHelper
import javax.inject.Inject


class PlayerActivity : BaseActivity(),
        PlayerContract.View,
        BottomNavigationView.OnNavigationItemSelectedListener {


    @Inject
    lateinit var presenter: PlayerContract.Presenter

    //region Views
    private lateinit var mBottomTab: BottomNavigationView
    private lateinit var mRelativeLayout: RelativeLayout
    private lateinit var mToolbar: Toolbar
    private lateinit var mAppBarLayout: AppBarLayout
    private lateinit var mViewPager: ViewPager
    //endregion

    private var onPageChanged: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            onNavigationItemSelected(mBottomTab.menu.getItem(position))
        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        mBottomTab = findViewById(R.id.player_bottom_nav)
        mRelativeLayout = findViewById(R.id.player_relative_layout)
        mToolbar = findViewById(R.id.toolbar)
        mAppBarLayout = findViewById(R.id.player_appbar_layout)
        mViewPager = findViewById(R.id.player_viewpager)
        mViewPager.offscreenPageLimit = 5
        setSupportActionBar(mToolbar)
        supportActionBar?.title = ""
        SupremeHelper.disableShiftMode(mBottomTab)
        mViewPager.adapter = PlayerTabViewPagerAdapter(supportFragmentManager)
        mBottomTab.setOnNavigationItemSelectedListener(this)
        onNavigationItemSelected(mBottomTab.menu.findItem(R.id.tab_global))
        presenter.getCurrentUser()
        presenter.getBuddies()
    }

    override fun onStart() {
        super.onStart()
        mViewPager.addOnPageChangeListener(onPageChanged)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        mViewPager.currentItem = item.order
        item.isChecked = true
        return true
    }

    override fun onStop() {
        presenter.stop()
        mViewPager.removeOnPageChangeListener(onPageChanged)
        super.onStop()
    }

}
