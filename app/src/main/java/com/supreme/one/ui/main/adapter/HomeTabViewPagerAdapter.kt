package com.supreme.one.ui.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.supreme.one.ui.main.tabs.account.AccountFragment
import com.supreme.one.ui.main.tabs.home.HomeFragment
import com.supreme.one.ui.main.tabs.playlist.PlaylistFragment
import com.supreme.one.ui.main.tabs.settings.SettingFragment
import com.supreme.one.ui.main.tabs.soundcloud.SoundCloudSearchFragment

/**
 * Created by Charlton on 3/2/18.
 */
class HomeTabViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val mFragments: Array<Fragment> = arrayOf(
            HomeFragment(),
            AccountFragment(),
            SoundCloudSearchFragment(),
            PlaylistFragment(),
            SettingFragment()
    )

    override fun getItem(position: Int): Fragment {
        return mFragments[position]
    }

    override fun getCount(): Int {
        return mFragments.size
    }

}