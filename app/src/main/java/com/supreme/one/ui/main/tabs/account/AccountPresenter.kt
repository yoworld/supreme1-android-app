package com.supreme.one.ui.main.tabs.account

import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.supreme.SupremeRepository

/**
 * Created by Charlton on 1/30/18.
 */

class AccountPresenter(val view: AccountContract.View,
                       val supremeRepository: SupremeRepository
) : BasePresenter<AccountContract.View>(view), AccountContract.Presenter {
    override fun getAccounts(user_id: Int) {
        supremeRepository.usersAccount(user_id).subscribe({ view.onAccountFetched(it.data!!) }, { view.onError(it) })
    }


}
