package com.supreme.one.ui.login

import android.os.Bundle
import android.util.Log
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.common.ConnectionResult
import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.network.UserService
import com.supreme.one.network.request.CredentialModel
import com.supreme.one.repository.user.UserRepository
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by Charlton on 1/1/18.
 */
class LoginPresenter(private var view: LoginContract.View, private var credentialService: UserRepository) : BasePresenter<LoginContract.View>(view), LoginContract.Presenter {
    override fun isLoggedIn(): Boolean {
        return credentialService.getSharedPreferenceManager().isLoggedIn()
    }

    override fun login(email: String, password: String) {
        addDisposable(
                credentialService.login(CredentialModel.login(email, password))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { response ->
                                    run {
                                        if (response.status) {
                                            val user = response.data
                                            view.onLoginSuccess(user!!)
                                        }
                                    }
                                },
                                { error -> view.onLoginFailed(error) },
                                { view.onLoginCompleted() })
        )
    }

    init {
        App.build.inject(this)
    }


    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if (connectionResult.isSuccess) {
            Log.e("SUCCESS", connectionResult.toString())
        } else {
            Log.e("FAILED", connectionResult.errorMessage)
            Log.e("FAILED ERROR CODE", connectionResult.errorCode.toString())
            view.onLoginFailed(Exception(connectionResult.errorMessage))
        }
    }

    override fun onConnected(p0: Bundle?) {


    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onCancel() {
        view.onLoginCanceled()
    }

    override fun onError(error: FacebookException?) {
        if (error != null) {
            view.onLoginFailed(error)
        }
    }

    override fun onSuccess(result: LoginResult?) {
        if (result != null) {
            facebook(result.accessToken.token)
        }
    }

    override fun facebook(access_token: String) {
        view.onLoginStarted()
        addDisposable(
                credentialService.facebook(CredentialModel.token(access_token))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { response ->
                                    run {
                                        if (response.status) {
                                            val user = response.data
                                            view.onLoginSuccess(user!!)
                                        }
                                    }
                                },
                                { error -> view.onLoginFailed(error) },
                                { view.onLoginCompleted() })
        )
    }


}