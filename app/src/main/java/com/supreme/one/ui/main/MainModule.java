package com.supreme.one.ui.main;


import com.supreme.one.repository.user.UserRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/1/18.
 */

@Module
public class MainModule {
    @Provides
    MainContract.Presenter providesMainPresenter(MainContract.View view, UserRepository service) {
        return new MainPresenter(view, service);
    }
}
