package com.supreme.one.ui.player.tabs.events

import android.content.Context
import android.os.Bundle
import androidx.appcompat.widget.AppCompatSpinner
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.ui.player.tabs.events.adapter.EventRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject


class EventFragment : BaseFragment(), EventContract.View {

    @Inject
    lateinit var presenter: EventContract.Presenter
    lateinit var mRecyclerView: RecyclerView
    lateinit var mSpinner: AppCompatSpinner


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_event, container, false)
        mRecyclerView = view.findViewById(R.id.event_recycler)
        mSpinner = view.findViewById(R.id.category_spinner)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView.addItemDecoration(GridSpacingItemDecoration(1, 10, false))
        mRecyclerView.adapter = EventRecyclerAdapter(sharedPreferenceManager, presenter)
        mSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position > -1){
                    (mRecyclerView.adapter as EventRecyclerAdapter).update(position)
                    presenter.changeEvent(position)
                }
             }

        }
        mSpinner.setSelection(0, true)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }

    override fun onChange() {
        Timber.e("Changing Event Category")
    }

    override fun onJoin() {
        Toast.makeText(context, "Joining...", Toast.LENGTH_SHORT).show()
    }

    override fun onError(throwable: Throwable) {
        Toast.makeText(context, "Error: " + throwable.message, Toast.LENGTH_LONG).show()
        Timber.e(throwable)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }


}

