package com.supreme.one.ui.player.tabs.room;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class RoomViewModule {

    @Binds
    abstract RoomContract.View provideFragmentRoomView(RoomFragment roomFragment);
}
