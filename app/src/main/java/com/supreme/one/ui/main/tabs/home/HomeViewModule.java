package com.supreme.one.ui.main.tabs.home;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class HomeViewModule {

    @Binds
    abstract HomeContract.View provideFragmentHomeView(HomeFragment homeFragment);
}
