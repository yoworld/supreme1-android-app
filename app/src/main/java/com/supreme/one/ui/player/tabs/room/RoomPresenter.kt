package com.supreme.one.ui.player.tabs.room

import com.supreme.one.App
import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.character.CharacterRepository

/**
 * Created by Charlton on 1/30/18.
 */

class RoomPresenter(val view: RoomContract.View,
                    val characterRepository: CharacterRepository
) : BasePresenter<RoomContract.View>(view), RoomContract.Presenter {

    init {
        App.build.inject(this)
    }


}
