package com.supreme.one.ui.main.tabs.account;

import com.supreme.one.repository.supreme.SupremeRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class AccountModule {
    @Provides
    AccountContract.Presenter providesAccountPresenter(AccountContract.View view, SupremeRepository repository) {
        return new AccountPresenter(view, repository);
    }
}
