package com.supreme.one.ui.player

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/1/18.
 */
interface PlayerContract {

    interface View : BaseView

    interface Presenter  {
        fun getCurrentUser()
        fun getBuddies();
        fun stop()
    }

}