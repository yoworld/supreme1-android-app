package com.supreme.one.ui.player.tabs.inbox;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class InboxViewModule {

    @Binds
    abstract InboxContract.View provideFragmenInboxView(InboxFragment inboxFragment);
}
