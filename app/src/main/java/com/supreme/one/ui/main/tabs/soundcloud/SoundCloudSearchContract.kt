package com.supreme.one.ui.main.tabs.soundcloud

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/30/18.
 */

interface SoundCloudSearchContract {
    interface View : BaseView {
        fun onError(throwable: Throwable)
    }

    interface Presenter {
        fun findSongs(query: String, page: Int = 1, per: Int = 100)
        fun addSong(user_id: Int, video_id: String)
        fun stop()
    }
}

