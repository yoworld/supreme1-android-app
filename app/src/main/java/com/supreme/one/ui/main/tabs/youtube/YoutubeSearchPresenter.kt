package com.supreme.one.ui.main.tabs.youtube

import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.youtube.YoutubeRepository

/**
 * Created by Charlton on 1/30/18.
 */

class YoutubeSearchPresenter(val view: YoutubeSearchContract.View,
                             val supremeRepository: YoutubeRepository
) : BasePresenter<YoutubeSearchContract.View>(view), YoutubeSearchContract.Presenter {

    override fun findVideos(query: String, page: Int, per: Int) {
        supremeRepository.findVideos(query, page, per).subscribe({}, { view.onError(it) })
    }

    override fun addSong(user_id: Int, video_id: String) {
        supremeRepository.addSong(user_id, object : HashMap<String, Any>() {
            init {
                put("video_id", video_id)
            }
        }).subscribe({}, { view.onError(it) })
    }

}
