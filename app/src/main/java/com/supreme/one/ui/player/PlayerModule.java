package com.supreme.one.ui.player;


import com.supreme.one.repository.user.UserRepository;
import com.supreme.one.ui.main.MainContract;
import com.supreme.one.ui.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/1/18.
 */

@Module
public class PlayerModule {
    @Provides
    PlayerContract.Presenter providesPlayerPresenter(PlayerContract.View view, UserRepository service) {
        return new PlayerPresenter(view, service);
    }
}
