package com.supreme.one.ui.player.tabs.room.adapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.supreme.one.R
import com.supreme.one.network.client.models.RawCharacter
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

/**
 * Created by Charlton on 3/3/18.
 */
class RoomRecyclerAdapter(data: OrderedRealmCollection<RawCharacter>?, var preferenceManager: SharedPreferenceManager? = null) : SupremeRealmRecyclerViewAdapter<RawCharacter, RoomRecyclerAdapter.CharacterHolder>(data, true)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterHolder {
        return CharacterHolder(LayoutInflater.from(parent.context!!).inflate(viewType, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_character_view
    }

    override fun onBindViewHolder(holder: CharacterHolder, position: Int) {
        val item = getItem(position)!!
        holder.characterName.text = item.name
        holder.roomName.text = String.format("Home: (%s), Room: (%s)", item.roomData!!.homeName, item.roomData!!.roomName)
        Glide.with(holder.itemView.context).load(item.getAvatar()).into(holder.characterAvatar)
    }

    class CharacterHolder(view: View) : RecyclerView.ViewHolder(view) {
        var characterName: AppCompatTextView = view.findViewById(R.id.holder_character_name)
        var roomName: AppCompatTextView = view.findViewById(R.id.holder_room_name)
        var characterAvatar: AppCompatImageView = view.findViewById(R.id.holder_character_avatar)

    }
}