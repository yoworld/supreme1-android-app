package com.supreme.one.ui.player.tabs.inbox.adapter

import android.content.res.ColorStateList
import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.supreme.one.R
import com.supreme.one.network.client.models.Message
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SupremeRealmRecyclerViewAdapter
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

/**
 * Created by Charlton on 2/8/18.
 */
class InboxRecyclerAdapter(data: OrderedRealmCollection<Message>?, var preferenceManager: SharedPreferenceManager? = null) : SupremeRealmRecyclerViewAdapter<Message, InboxRecyclerAdapter.MessageHolder>(data, true) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {
        return when (viewType) {
            R.layout.holder_inbox_layout -> MessageHolder(LayoutInflater.from(parent!!.context).inflate(viewType, parent, false))
            else -> MessageHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
        }
    }

    override fun onBindViewHolder(holder: MessageHolder, position: Int) {
        val item = getItem(position)!!
        val textColor = item.getContrastColor(item.getColorHexInt())
        val colorStateList = ColorStateList.valueOf(item.getColorHexIntAlpha()) //ContextCompat.getColorStateList(holder.itemView.context, R.color.grey)

        if (item.fromPlayerId == preferenceManager?.getAccountId()) {
            holder.itemView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        } else {
            holder.itemView.layoutDirection = View.LAYOUT_DIRECTION_LTR
        }



        when {
            item.chatType == Message.PUBLIC  || item.chatType == Message.PRIVATE -> {
                holder.messageContent.setTextColor(Color.BLACK)
                holder.messageTime.setTextColor(Color.BLACK)

            }
            item.chatType == Message.SHOUT -> {
                holder.messageContent.setTextColor(Color.BLACK)
                holder.messageTime.setTextColor(Color.BLACK)

            }
            item.chatType == Message.GLOBAL -> {
                holder.messageContent.setTextColor(Color.BLACK)
                holder.messageTime.setTextColor(Color.BLACK)

            }
        }


        holder.messageTime.text = DateUtils.getRelativeTimeSpanString(item.createdAt)
        holder.messageContent.text = item.filteredMessage(holder.itemView.context)
        holder.messageAuthor.text = item.fromPlayerName
        Glide.with(holder.itemView.context).asBitmap().load(item.getAvatar()).into(holder.messageAvatar)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.holder_inbox_layout
    }

    class MessageHolder(view: View) : RecyclerView.ViewHolder(view) {

        var messageLayout: LinearLayout = view.findViewById(R.id.holder_message_layout)
        var messageContent: AppCompatTextView = view.findViewById(R.id.holder_message)
        var messageAvatar: AppCompatImageView = view.findViewById(R.id.holder_message_avatar)
        var messageAuthor: AppCompatTextView = view.findViewById(R.id.holder_message_name)
        var messageTime: AppCompatTextView = view.findViewById(R.id.holder_message_time)
    }


}