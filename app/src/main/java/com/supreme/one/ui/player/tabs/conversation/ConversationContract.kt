package com.supreme.one.ui.player.tabs.conversation

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/30/18.
 */

interface ConversationContract {
    interface View : BaseView {
        fun onMessageSent()

        fun onError(throwable: Throwable)

    }

    interface Presenter {
        fun sendMessage(message: String)
        fun stop()
    }
}

