package com.supreme.one.ui.main.tabs.playlist

import android.annotation.SuppressLint
import com.supreme.one.base.BasePresenter
import com.supreme.one.repository.soundcloud.SoundCloudRepository
import com.supreme.one.repository.youtube.YoutubeRepository

/**
 * Created by Charlton on 1/30/18.
 */

class PlaylistPresenter(val view: PlaylistContract.View,
                        val youtubeRepository: YoutubeRepository,
                        val soundCloudRepository: SoundCloudRepository
) : BasePresenter<PlaylistContract.View>(view), PlaylistContract.Presenter {

    @SuppressLint("CheckResult")
    override fun getYoutubePlaylist(user_id: Int, page: Int, per: Int) {
        youtubeRepository.getUserPlaylist(user_id, page, per).subscribe({ view.onYoutubePlaylistFetched(it.data!!) }, { view.onError(it) })
    }

    override fun getSoundCloudPlaylist(user_id: Int, page: Int, per: Int) {
        soundCloudRepository.getUserPlaylist(user_id, page, per).subscribe({ view.onSoundCloudPlaylistFetched(it.data!!) }, { view.onError(it) })
    }


}
