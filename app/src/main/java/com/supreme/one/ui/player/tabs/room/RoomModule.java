package com.supreme.one.ui.player.tabs.room;

import com.supreme.one.repository.character.CharacterRepository;
import com.supreme.one.ui.player.tabs.conversation.ConversationContract;
import com.supreme.one.ui.player.tabs.conversation.ConversationPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class RoomModule {
    @Provides
    RoomContract.Presenter providesRoomPresenter(RoomContract.View view, CharacterRepository repository) {
        return new RoomPresenter(view, repository);
    }
}
