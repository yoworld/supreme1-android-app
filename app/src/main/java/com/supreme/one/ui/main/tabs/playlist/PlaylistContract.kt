package com.supreme.one.ui.main.tabs.playlist

import com.supreme.one.base.BaseView
import com.supreme.one.network.models.SoundCloudPlaylistModel
import com.supreme.one.network.models.YoutubePlaylistModel

/**
 * Created by Charlton on 1/30/18.
 */

interface PlaylistContract {
    interface View : BaseView {
        fun onYoutubePlaylistFetched(user: List<YoutubePlaylistModel>)
        fun onSoundCloudPlaylistFetched(user: List<SoundCloudPlaylistModel>)

        fun onError(throwable: Throwable)
    }

    interface Presenter {
        fun getYoutubePlaylist(user_id: Int, page: Int = 1, per: Int = 20)
        fun getSoundCloudPlaylist(user_id: Int, page: Int = 1, per: Int = 20)

        fun stop()
    }
}

