package com.supreme.one.ui.player.tabs.buddy

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.client.models.Buddy
import com.supreme.one.ui.player.tabs.buddy.adapter.BuddyRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import io.realm.Realm
import timber.log.Timber
import javax.inject.Inject


class BuddyFragment : BaseFragment(), BuddyContract.View {

    @Inject
    lateinit var presenter: BuddyContract.Presenter
    lateinit var mRecyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_buddy, container, false)
        mRecyclerView = view.findViewById(R.id.buddy_recycler)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView.addItemDecoration(GridSpacingItemDecoration(1, 10, false))
        mRecyclerView.adapter = BuddyRecyclerAdapter(Realm.getDefaultInstance().where(Buddy::class.java).findAllAsync(), sharedPreferenceManager)
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }

    override fun onUpdating() {
        Toast.makeText(context, "Updating List....", Toast.LENGTH_SHORT).show()
    }

    override fun onError(throwable: Throwable) {
        Toast.makeText(context, "Error: " + throwable.message, Toast.LENGTH_LONG).show()
        Timber.e(throwable)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

}

