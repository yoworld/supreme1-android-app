package com.supreme.one.ui.main

import android.content.res.ColorStateList
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.supreme.one.R
import com.supreme.one.Supreme
import com.supreme.one.base.BaseActivity
import com.supreme.one.network.models.UserModel
import com.supreme.one.ui.main.adapter.HomeTabViewPagerAdapter
import com.supreme.one.util.SupremeHelper
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MainActivity : BaseActivity(),
        MainContract.View,
        BottomNavigationView.OnNavigationItemSelectedListener {


    @Inject
    lateinit var presenter: MainContract.Presenter

    //region Views
    lateinit var mBottomTab: BottomNavigationView
    lateinit var mRelativeLayout: RelativeLayout
    lateinit var mToolbar: Toolbar
    lateinit var mAppBarLayout: AppBarLayout
    lateinit var mViewPager: ViewPager
    lateinit var mCloud: AppCompatImageView
    //endregion

    var onPageChanged: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            onNavigationItemSelected(mBottomTab.menu.getItem(position))
        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        mBottomTab = findViewById(R.id.home_bottom_nav)
        mRelativeLayout = findViewById(R.id.home_relative_layout)
        mToolbar = findViewById(R.id.toolbar)
        mAppBarLayout = findViewById(R.id.home_appbar_layout)
        mViewPager = findViewById(R.id.home_viewpager)
        mCloud = findViewById(R.id.cloud)

        setSupportActionBar(mToolbar)
        supportActionBar?.title = ""
        //SupremeHelper.disableShiftMode(mBottomTab)
        mViewPager.adapter = HomeTabViewPagerAdapter(supportFragmentManager)
        mBottomTab.setOnNavigationItemSelectedListener(this)
        onNavigationItemSelected(mBottomTab.menu.findItem(R.id.tab_home))
        presenter.getCurrentUser()
    }

    override fun onConnected(message: String){
        super.onConnected(message)
        mCloud.setImageResource(R.drawable.ic_112_cloud)
        mCloud.imageTintList = ContextCompat.getColorStateList(this, R.color.light_green)
    }

    override fun onRetrying(message: String) {
        super.onRetrying(message)
        mCloud.setImageResource(R.drawable.ic_113_cloud_computing)
        mCloud.imageTintList = ContextCompat.getColorStateList(this, R.color.light_red)
    }

    override fun onDisconnected(message: String) {
        mCloud.setImageResource(R.drawable.ic_113_cloud_computing)
    }

    override fun onStart() {
        super.onStart()
        mAppClient.start()
        mViewPager.addOnPageChangeListener(onPageChanged)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        mViewPager.currentItem = item.order
        item.isChecked = true
        return true
    }

    override fun onStop() {
        presenter.stop()
        mViewPager.removeOnPageChangeListener(onPageChanged)
        super.onStop()
    }


}
