package com.supreme.one.ui.player.tabs.inbox

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.supreme.one.R
import com.supreme.one.base.BaseFragment
import com.supreme.one.network.client.models.Message
import com.supreme.one.ui.player.tabs.conversation.adapter.ConversationRecyclerAdapter
import com.supreme.one.ui.player.tabs.inbox.adapter.InboxRecyclerAdapter
import com.supreme.one.util.GridSpacingItemDecoration
import dagger.android.support.AndroidSupportInjection
import io.realm.Realm
import io.realm.Sort
import timber.log.Timber
import javax.inject.Inject


class InboxFragment : BaseFragment(), InboxContract.View {

    @Inject
    lateinit var presenter: InboxContract.Presenter
    private lateinit var mRecyclerView: RecyclerView

    private var mAdapterObserver: RecyclerView.AdapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            val item = (mRecyclerView.adapter as ConversationRecyclerAdapter).getItem(mRecyclerView.adapter!!.itemCount - 1)!!
            val textColor = item.getContrastColor(item.getColorHexInt())
            val colorStateList = item.getColorHexInt() //ContextCompat.getColorStateList(holder.itemView.context, R.color.grey)
            scrollToLatestMessage(mRecyclerView, String.format("PRIVATE MSG: %s - %s", item.fromPlayerName, item.filteredMessage(context)), color = textColor, bg = colorStateList)
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            super.onItemRangeRemoved(positionStart, itemCount)
            val item = (mRecyclerView.adapter as ConversationRecyclerAdapter).getItem(mRecyclerView.adapter!!.itemCount - 1)!!
            val textColor = item.getContrastColor(item.getColorHexInt())
            val colorStateList = item.getColorHexInt() //ContextCompat.getColorStateList(holder.itemView.context, R.color.grey)
            scrollToLatestMessage(mRecyclerView, String.format("PRIVATE MSG: %s - %s", item.fromPlayerName, item.filteredMessage(context)), color = textColor, bg = colorStateList)
        }
    }

    override fun onError(throwable: Throwable) {
        Toast.makeText(context, "Error: " + throwable.message, Toast.LENGTH_LONG).show()
        Timber.e(throwable)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_inbox, container, false)
        mRecyclerView = view.findViewById(R.id.inbox_recycler)
        mRecyclerView.addItemDecoration(GridSpacingItemDecoration(1, 5, false))
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView.adapter = InboxRecyclerAdapter(
                Realm.getDefaultInstance().where(Message::class.java)
                        .equalTo("chatType", Message.PRIVATE)
                        .notEqualTo("fromPlayerId", super.sharedPreferenceManager.getAccountId())
                        .sort("createdAt", Sort.ASCENDING)
                        .distinct("fromPlayerId")
                        .findAllAsync(),
                sharedPreferenceManager
        )
        (mRecyclerView.adapter as InboxRecyclerAdapter?)?.registerAdapterDataObserver(mAdapterObserver)
    }

    override fun onDestroyView() {
        presenter.stop()
        mRecyclerView.adapter?.unregisterAdapterDataObserver(mAdapterObserver)
        super.onDestroyView()
    }

    override fun onMessageSent() {

    }

}

