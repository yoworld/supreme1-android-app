package com.supreme.one.ui.player.tabs.conversation;

import com.supreme.one.repository.character.CharacterRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public class ConversationModule {
    @Provides
    ConversationContract.Presenter providesConversationPresenter(ConversationContract.View view, CharacterRepository repository) {
        return new ConversationPresenter(view, repository);
    }
}
