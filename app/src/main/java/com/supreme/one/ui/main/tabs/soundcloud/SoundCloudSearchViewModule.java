package com.supreme.one.ui.main.tabs.soundcloud;



import dagger.Binds;
import dagger.Module;

/**
 * Created by Charlton on 1/30/18.
 */

@Module
public abstract class SoundCloudSearchViewModule {

    @Binds
    abstract SoundCloudSearchContract.View provideFragmentYoutubeSearchView(SoundCloudSearchFragment playlistFragment);
}
