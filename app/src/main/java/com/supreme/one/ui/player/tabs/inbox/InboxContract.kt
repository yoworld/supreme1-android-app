package com.supreme.one.ui.player.tabs.inbox

import com.supreme.one.base.BaseView

/**
 * Created by Charlton on 1/30/18.
 */

interface InboxContract {
    interface View : BaseView {
        fun onError(throwable: Throwable)
        fun onMessageSent();
    }

    interface Presenter {
        fun stop()
        fun sendMessage(to: Int,  message: String)
    }
}

