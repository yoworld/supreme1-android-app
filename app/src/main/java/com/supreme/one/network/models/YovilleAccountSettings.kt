package com.supreme.one.network.models

import io.realm.RealmList
import io.realm.RealmObject

open class YovilleAccountSettings : RealmObject() {
    var ignore: RealmList<Int> = RealmList()
    var copycat: Boolean = false
    var rainbow: Boolean = false
    var spoofer: Boolean = false
    var stalker: Boolean = false
    var canfreeze: Boolean = false
    var chaosmode: Boolean = false
    var canttt: Boolean = false
    var laglessclothes: Boolean = false
    var laglessroom: Boolean = false
    var petchat: Boolean = false
    var cancorruptgb: Boolean = false
    var frequency: Int = 0
    var lfrequency: Int = -1
    var supremeentrance: Boolean = false
    var id: Int = 0

}
