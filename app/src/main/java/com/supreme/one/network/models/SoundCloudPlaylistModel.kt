package com.supreme.one.network.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */
open class SoundCloudPlaylistModel : RealmObject() {

    var user_id: Int = 0
    var track_id: Int = 0
    var permalink: String? = null
    var position: Int? = 0
    var created_at: Date? = null
    var updated_at: Date? = null
    var title: String? = null
    var jpg_link: String? = null
    var mp3_link: String? = null
    @PrimaryKey
    var composite_key: String? = null


    fun getSoundCloudLink(): String {
        return String.format("https://soundcloud.com/%s", this.permalink)
    }

}