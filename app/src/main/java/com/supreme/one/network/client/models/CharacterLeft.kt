package com.supreme.one.network.client.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 3/3/18.
 */

open class CharacterLeft : RealmObject() {
    @PrimaryKey
    var playerId: Int = 0
    var characterName: String? = null
    var isViking: Boolean = false
    var speechBalloonColor: Int = 0
    var gender: Int = 0
    var isVIP: Boolean = false
}
