package com.supreme.one.network.client.models

import com.supreme.one.util.SupremeHelper
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Charlton on 2/28/18.
 */


open class Event : RealmObject() {
    var desc: String? = null
    var createdSeconds: Int = 0
    var category: Int = 0
    var playerHomeId: String? = null
    var time: String? = null
    @PrimaryKey
    var pid: Int = 0
    var premium: Int = 0
    var created: String? = null
    var isBuddy: Boolean = false
    var timeSeconds: Int = 0
    var createdBy: String? = null
    var playerRoomId: String? = null
    var avatarUrl: String? = null
    var visitors: Int = 0
    var instance: Int = 0
    var title: String? = null
    var id: Int = 0
    var currentStamp: Long = 0
    var expires: Long = 0


    fun getPlayerAvatarUrl() : String {
        return SupremeHelper.getAvatarUrlFromId(pid)
    }

    fun join(): Room {
        val room = Room()
        room.homeId = playerHomeId?.toInt()?:0
        room.homeName = title
        room.ownerName = createdBy
        room.zoneName = "h$playerRoomId"
        room.roomId = playerRoomId?.toInt()?:0
        room.instanceId = instance
        room.roomName = title
        room.ownerPlayerId = pid
        room.zoneOwnerId = pid
        room.serverZoneName = "h$playerRoomId"
        room.requestId = id.toString()
        return room
    }
}
