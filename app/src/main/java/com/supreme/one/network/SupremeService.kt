package com.supreme.one.network

import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.SupremeAccount

import java.util.ArrayList

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface SupremeService {


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/user/{id}/account")
    fun usersAccount(@Path("id") id: Int): Observable<YoResponse<ArrayList<SupremeAccount>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/supreme/{id}")
    operator fun get(@Path("id") id: Int): Observable<YoResponse<SupremeAccount, String>>

}
