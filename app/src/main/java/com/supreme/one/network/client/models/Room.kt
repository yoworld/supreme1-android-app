package com.supreme.one.network.client.models

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 2/28/18.
 */

open class Room : RealmObject(){
    var positionIndex: Int = 0
    @PrimaryKey
    var roomId: Int = 0
    var instanceId: Int = 0
    var allowEntranceAction: Boolean = false
    var ownerName: String? = null
    var homeId: Int = 0
    var roomName: String? = null
    var homeItemId: Int = 0
    var teleportId: Int = 0
    var serverZoneName: String? = null
    var ownerPlayerId: Int = 0
    var partyId: Int = 0
    var homeName: String? = null
    var zoneName: String? = null
    var requestId: String? = null
    var browseContest: Boolean = false
    @io.realm.annotations.Ignore
    var forPlayerId: Int = 0
    @io.realm.annotations.Ignore
    var zoneOwnerId: Int = 0

    @LinkingObjects("roomData")
    private val current: RealmResults<RawCharacter>? = null
}
