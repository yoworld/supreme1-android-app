package com.supreme.one.network

import com.supreme.one.Supreme
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.BuddyModel
import io.reactivex.Observable
import retrofit2.http.*
import java.util.ArrayList
import java.util.HashMap

/**
 * Created by Charlton on 2/7/18.
 */
interface BuddyService {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/supreme/{id}/buddy")
    fun search(@Path("id") id: Int, @QueryMap query: HashMap<String, Any>): Observable<YoResponse<ArrayList<BuddyModel>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("/api/v1/supreme/{user_id}/buddy/{buddy_id}")
    fun update(@Path("user_id") id: Int, @Path("buddy_id") buddy_id: Int, @Body query: HashMap<String, Any>): Observable<YoResponse<BuddyModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("/api/v1/supreme/{user_id}/buddy/{buddy_id}")
    fun delete(@Path("user_id") user_id: Int, @Path("buddy_id") buddy_id: Int): Observable<YoResponse<BuddyModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/supreme/{user_id}/buddy/{buddy_id}")
    operator fun get(@Path("user_id") user_id: Int, @Path("buddy_id") buddy_id: Int): Observable<YoResponse<BuddyModel, PaginationData>>

}