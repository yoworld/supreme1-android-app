package com.supreme.one.network.client

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.supreme.one.Supreme
import com.supreme.one.network.client.models.*
import com.supreme.one.util.SharedPreferenceManager
import com.supreme.one.util.SupremeHelper
import io.realm.Realm
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketException


/**
 * Created by Charlton on 2/28/18.
 */

class AppClient(
        private val context: Context,
        private val preferences: SharedPreferenceManager,
        private var hostAddress: String = "s1.supreme-one.net",
        private var portNumber: Int = 2002,
        private var log: Boolean = true,
        private var listener: AppClientConnectionListener? = null
) : Runnable {


    //region Set Functions
    fun playMusic(message: Any) {
        write(BigBrotherResponse.create<SendMessage, Void>()
                .setMessage("Chat Message").setType("message")
                .setData(SendMessage("public", String.format("<play> %s", message))))
    }

    fun getBuddies(): Boolean {
        return write(BigBrotherResponse.create<Void, Void>()
                .setMessage("buddies_update").setType("buddies_update"))
    }

    fun getCategory(category: Int): Boolean {
        return write(BigBrotherResponse.create<Int, Void>()
                .setMessage("change_category").setType("events")
                .setData(category))
    }

    fun sendMessage(message: String): Boolean {
        return write(BigBrotherResponse.create<SendMessage, Void>()
                .setMessage("Chat Message").setType("message")
                .setData(SendMessage("public", message)))
    }

    fun sendPrivateMessage(serverUserId: Int, message: String): Boolean {
        return write(BigBrotherResponse.create<SendMessage, Void>()
                .setMessage("Chat Message").setType("message")
                .setData(SendMessage("private", message, serverUserId)))
    }

    fun sendShoutMessage(serverUserId: Int, message: String): Boolean {
        return write(BigBrotherResponse.create<SendMessage, Void>()
                .setMessage("Chat Message").setType("message")
                .setData(SendMessage("shout", message, serverUserId)))
    }


    fun sendGlobalMessage(serverUserId: Int, message: String): Boolean {
        return write(BigBrotherResponse.create<SendMessage, Void>()
                .setMessage("Chat Message").setType("message")
                .setData(SendMessage("global", message, serverUserId)))
    }

    //endregion

    fun stop() {
        thread.interrupt()
        pingThread.interrupt()
        socket?.close()
    }

    fun start(): Boolean {
        Timber.w("STARTED CLIENT")
        if (!thread.isAlive) {
            thread.start()
            return true
        }
        return false
    }

    fun isConnected(): Boolean = socket != null && !socket!!.isClosed && !socket!!.isInputShutdown && !socket!!.isOutputShutdown
    fun isClosed(): Boolean = if (socket != null) socket!!.isClosed else true
    fun setHostAndPort(hostAddress: String = "s1.supreme-one.net", portNumber: Int = 2002) {
        this.hostAddress = hostAddress
        this.portNumber = portNumber
    }

    private fun <D, E> write(bigBrotherResponse: BigBrotherResponse<D, E>): Boolean {
        try {
            bigBrotherResponse.auth = SupremeAuth().setS1Token(preferences.getToken()).setS1TokenAcc(preferences.getAccountToken())
            val data = GsonBuilder().disableHtmlEscaping().create().toJson(bigBrotherResponse, object : TypeToken<BigBrotherResponse<D, E>>() {}.type) + "\n"
            socket!!.outputStream.write(data.toByteArray())
            socket!!.outputStream.flush()
            return true
        } catch (e: Exception) {
            Timber.e(e, "Unable to send message")
        }
        return false
    }

    private fun authorize() {
        val value = String.format(FULL_AUTH, preferences.getToken(), preferences.getAccountToken())
        socket!!.outputStream.write(value.toByteArray())
        socket!!.outputStream.flush()
    }

    private fun connect(inetSocketAddress: InetSocketAddress = InetSocketAddress(hostAddress, portNumber), timeout: Int = 5000) {
        Timber.w("INITIALIZING CLIENT")
        socket = Socket()
        Timber.w("CONNECTING TO CLIENT")
        socket!!.connect(inetSocketAddress, timeout)
        Timber.w("AUTHORIZING CLIENT")
        authorize()
        Timber.w("NOTIFYING CONNECTION")
        listener?.onClientConnected()
    }

    private fun available(): Int {
        return socket!!.getInputStream().available()
    }

    private var runnable: Runnable = Runnable {
        while (!pingThread.isInterrupted) {
            try {
                Thread.sleep(5000)
                if (!write(BigBrotherResponse.create<String, String>().setType("ping").setMessage("keep-alive"))) {
                    listener?.onClientRetrying()
                    connect()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        listener?.onClientDisconnect()
    }
    private var thread: Thread = Thread(this)
    private var socket: Socket? = null
    private var pingThread: Thread = Thread(runnable)

    override fun run() {
        while (socket == null || !socket!!.isConnected) {
            try {
                connect()
                Thread.sleep(5000)
            } catch (e: Exception) {
                Timber.e("Unable to connect, Connection might be refused? ${e.message}")
            }
        }
        if (!pingThread.isAlive) pingThread.start()
        while (!thread.isInterrupted) {
            try {
                if (isConnected()) {
                    val br = socket!!.getInputStream().bufferedReader()
                    val message = br.readLine()
                    if (message.trim().isNotEmpty()) {
                        try {
                            val jsonObject = JSONObject(message)
                            if (log && jsonObject.getString("type") != "pong") Timber.w("Swf Said =>\n%s", jsonObject.toString(4))
                            when (jsonObject.getString("type")) {
                                "events" -> {
                                    val type = object : TypeToken<BigBrotherResponse<ArrayList<Event>, String>>() {}.type
                                    val json = Gson().fromJson<BigBrotherResponse<ArrayList<Event>, String>>(message, type)
                                    val stamp = System.currentTimeMillis()

                                    json.data.forEach {
                                        val expires = stamp + (it.timeSeconds * 1000)
                                        it.currentStamp = stamp
                                        it.expires = expires
                                    }
                                    Realm.getDefaultInstance().executeTransaction { it.copyToRealmOrUpdate(json.data) }
                                    Supreme.events.onNext(json)
                                }
                                "message" -> {
                                    val type = object : TypeToken<BigBrotherResponse<Message, String>>() {}.type
                                    val json = Gson().fromJson<BigBrotherResponse<Message, String>>(message, type)
                                    Realm.getDefaultInstance().executeTransaction {
                                        val message1 = json.data.message!!
                                        if (message1.startsWith("C::")) {
                                            val where = it.where(Message::class.java)
                                            if (where.count() > 0) {
                                                val findFirst = where.sort("createdAt").findAll().last()
                                                if (findFirst != null) {
                                                    if (findFirst.message!!.startsWith("C::")) {
                                                        if (findFirst.fromPlayerId == json.data.fromPlayerId) {
                                                            findFirst.message += "\n${json.data.message}"
                                                            it.insertOrUpdate(findFirst)
                                                            return@executeTransaction
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        it.copyToRealmOrUpdate(json.data)
                                    }
                                    Supreme.message.onNext(json)
                                }
                                "room" -> {
                                    val type = object : TypeToken<BigBrotherResponse<Room, String>>() {}.type
                                    val fromJson = Gson().fromJson<BigBrotherResponse<Room, String>>(message, type)
                                    preferences.saveRoomId(fromJson.data.roomId)
                                    Realm.getDefaultInstance().executeTransaction {
                                        it.delete(RawCharacter::class.java)
                                    }
                                    Supreme.room.onNext(fromJson)
                                }
                                "character_entered" -> {
                                    val type = object : TypeToken<BigBrotherResponse<RawCharacter, String>>() {}.type
                                    val fromJson = Gson().fromJson<BigBrotherResponse<RawCharacter, String>>(message, type)
                                    Realm.getDefaultInstance().executeTransaction {
                                        it.copyToRealmOrUpdate(fromJson.data)
                                    }
                                    Supreme.characterEntered.onNext(fromJson)
                                }
                                "character_left" -> {
                                    val type = object : TypeToken<BigBrotherResponse<CharacterLeft, String>>() {}.type
                                    val fromJson = Gson().fromJson<BigBrotherResponse<CharacterLeft, String>>(message, type)
                                    val findFirst = Realm.getDefaultInstance().where(RawCharacter::class.java)
                                            .equalTo("playerId", fromJson.data.playerId)
                                            .findFirst()
                                    if (findFirst != null) {
                                        Realm.getDefaultInstance().executeTransaction {
                                            findFirst.deleteFromRealm()
                                        }
                                    }
                                    Supreme.characterLeft.onNext(fromJson)
                                }
                                "buddies_update" -> {
                                    val string = jsonObject.getString("data")
                                    val bitShift = SupremeHelper.bitShift(string, 2)
                                    if (log) Timber.e("JSON => \n$bitShift")
                                    val json = JSONObject(bitShift)
                                    jsonObject.put("data", json)
                                    val type = object : TypeToken<BigBrotherResponse<BuddyWrapper, String>>() {}.type
                                    val fromJson = Gson().fromJson<BigBrotherResponse<BuddyWrapper, String>>(jsonObject.toString(), type)
                                    if (fromJson != null) {
                                        Realm.getDefaultInstance().executeTransaction {
                                            if (fromJson.data.buddies != null)
                                                it.copyToRealmOrUpdate(fromJson.data.buddies!!)
                                        }
                                    }
                                    Supreme.buddyUpdated.onNext(fromJson)
                                }
                                "error" -> {
                                    Supreme.connectSwf.onNext(jsonObject.getString("message"))
                                }
                                "sync" -> { }
                            }
                        } catch (e: JSONException) {
                            Timber.e(e, "An Error Occurred With the Json Format =>\n${e.message}=>\n$message")
                        }
                    } else {
                        if (log) Timber.w("Swf Said =>\n$message")
                    }
                }
            } catch (e: SocketException) {
                Thread.sleep(5000)
            } catch (e: Exception) {
                Timber.e(e, "SOME BULL SHIT OCCURRED WITH SOCKET: ${e.message}")
            }
        }
        listener?.onClientDisconnect()
    }


    companion object {
        private const val FULL_AUTH = "{\"type\": \"init\", \"message\": \"app\", \"auth\": { \"s1_token\": \"%s\", \"s1_token_acc\": \"%s\" }}\n"
    }

    interface AppClientConnectionListener {
        fun onClientConnected()
        fun onClientDisconnect()
        fun onClientRetrying()
    }

    fun refreshBuddyList(): Boolean {
        return true
    }

    fun join(join: Room): Boolean {
        return write(BigBrotherResponse.create<Room, Void>()
                .setMessage("Joining room").setType("join_room")
                .setData(join))
    }

}
