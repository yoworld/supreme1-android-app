package com.supreme.one.network.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 2/7/18.
 */

open class YoutubeInfoModel : RealmObject() {

    @PrimaryKey
    var id: String? = null
    lateinit var link: String
    lateinit var title: String
    lateinit var mp4_md5_hash: String
    lateinit var mp3_md5_hash: String
    lateinit var mp4_loc: String
    lateinit var mp3_loc: String
    lateinit var created_at: String
    lateinit var updated_at: String
    lateinit var mp3_link: String

}
