package com.supreme.one.network

import com.supreme.one.network.models.YovilleAccountSettings
import io.reactivex.Observable
import retrofit2.http.*
import java.util.HashMap

/**
 * Created by Charlton on 2/7/18.
 */
interface SettingService {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("/api/v1/settings/{id}?decode=true")
    fun settings(@Path("id") id: Int, @Body query: HashMap<String, Any>): Observable<YoResponse<YovilleAccountSettings, String>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("/api/v1/settings/{id}?decode=true")
    fun settings(@Path("id") id: Int, @Body query: YovilleAccountSettings): Observable<YoResponse<YovilleAccountSettings, String>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/settings/{id}?decoded=true")
    fun settings(@Path("id") id: Int): Observable<YoResponse<YovilleAccountSettings, String>>

}