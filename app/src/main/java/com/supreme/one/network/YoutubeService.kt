package com.supreme.one.network

import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.YoutubeInfoModel
import com.supreme.one.network.models.YoutubePlaylistModel
import com.supreme.one.network.models.YoutubeResult
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by Charlton on 2/7/18.
 */
interface YoutubeService {



    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/youtube")
    fun findVideos(@Query("query") query: String, @Query("page") page: Int = 1, @Query("per") per: Int = 50): Observable<YoResponse<ArrayList<YoutubeResult>, PaginationData>>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/mp3")
    fun getSongs(@Query("title") query: String, @Query("page") page: Int = 1, @Query("per") per: Int = 20): Observable<YoResponse<ArrayList<YoutubeInfoModel>, PaginationData>>

    @Headers("Content-Type: audio/mpeg", "Accept: audio/mpeg")
    @GET("/api/v1/mp3/{vid}")
    fun getYoutubeSong(@Path("vid") query: String): Observable<ResponseBody>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/user/{id}/playlist")
    fun getUserPlaylist(@Path("id") id: Int, @Query("page") page: Int = 1, @Query("per") per: Int = 20): Observable<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/playlist")
    fun getCurrentPlaylist(): Observable<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("/api/v1/user/{id}/playlist/{vid}")
    fun removeSong(@Path("id") id: Int, @Path("vid") vid: String): Observable<YoResponse<String, String>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("/api/v1/user/{id}/playlist/{vid}")
    fun updateSong(@Path("id") id: Int, @Path("vid") vid: String, @Body map: HashMap<String, Any> = object: HashMap<String, Any>(){
        init {
            put("position",0)
        }
    }): Observable<YoResponse<YoutubePlaylistModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("/api/v1/user/{id}/playlist")
    fun addSong(@Path("id") id: Int, @Body map: HashMap<String, Any>): Observable<YoResponse<YoutubePlaylistModel, PaginationData>>

}