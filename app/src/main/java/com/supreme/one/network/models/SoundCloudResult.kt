package com.supreme.one.network.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Charlton on 2/10/18.
 */
open class SoundCloudResult: RealmObject() {
    @PrimaryKey
    var track_id: Int? = null
    var username: String? = null
    var artist: String? = null
    var title: String? = null
    var description: String? = null
    var genre: String? = null
    var link: String? = null
    var relevance: Double? = null
    var mp3_link: String? = null
    var jpg_link: String? = null
    var permalink: String? = null
}