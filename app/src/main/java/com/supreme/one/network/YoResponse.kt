package com.supreme.one.network

class YoResponse<D, E> {

    constructor()

    constructor(status: Boolean, message: String, data: D, extra: E){
        this.status = status
        this.message = message
        this.data = data
        this.extra = extra
    }
    var status: Boolean = false
    var message: String? = null
    var data: D? = null
    var extra: E? = null


}
