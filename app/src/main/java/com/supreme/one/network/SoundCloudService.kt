package com.supreme.one.network

import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.SoundCloudPlaylistModel
import com.supreme.one.network.models.SoundCloudResult
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */
interface SoundCloudService {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/soundcloud")
    fun findSongs(@Query("query") query: String, @Query("page") page: Int = 1, @Query("per") per: Int = 50): Observable<YoResponse<ArrayList<SoundCloudResult>, PaginationData>>

    @Headers("Content-Type: audio/mpeg", "Accept: audio/mpeg")
    @GET("/api/v1/soundcloud/{permalink}")
    fun getSoundCloudSong(@Path("permalink") query: String): Observable<ResponseBody>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/user/{id}/playlist")
    fun getUserPlaylist(@Path("id") id: Int, @Query("page") page: Int = 1, @Query("per") per: Int = 20): Observable<YoResponse<ArrayList<SoundCloudPlaylistModel>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/playlist")
    fun getCurrentPlaylist(): Observable<YoResponse<ArrayList<SoundCloudPlaylistModel>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("/api/v1/user/{id}/playlist/{track_id}")
    fun removeSong(@Path("id") id: Int, @Path("track_id") track_id: Int): Observable<YoResponse<String, String>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("/api/v1/user/{id}/playlist/{track_id}")
    fun updateSong(@Path("id") id: Int, @Path("track_id") track_id: Int, @Body map: HashMap<String, Any> = object: HashMap<String, Any>(){
        init {
            put("position",0)
        }
    }): Observable<YoResponse<SoundCloudPlaylistModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("/api/v1/user/{id}/playlist")
    fun addSong(@Path("id") id: Int, @Body map: HashMap<String, Any>): Observable<YoResponse<SoundCloudPlaylistModel, PaginationData>>

}