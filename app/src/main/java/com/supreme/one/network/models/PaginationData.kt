package com.supreme.one.network.models

class PaginationData {
    var current_page: Int = 0
    lateinit var first_page_url: String
    var from: Int = 0
    var last_page: Int = 0
    lateinit var last_page_url: String
    lateinit var next_page_url: String
    lateinit var path: String
    lateinit var per_page: String
    lateinit var prev_page_url: String
    var to: Int = 0
    var total: Int = 0

}
