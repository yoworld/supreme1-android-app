package com.supreme.one.network.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Charlton on 2/28/18.
 */

public class BigBrotherResponse<D, E>{
    @SerializedName("type")
    String type;
    @SerializedName("message")
    String message;
    @SerializedName("auth")
    SupremeAuth auth;
    @SerializedName("data")
    D data;
    @SerializedName("extra")
    E extra;

    public static <D, E> BigBrotherResponse create(){
        return new BigBrotherResponse<D, E>();
    }


    public BigBrotherResponse setType(String type) {
        this.type = type;
        return this;
    }

    public BigBrotherResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public BigBrotherResponse setAuth(SupremeAuth auth) {
        this.auth = auth;
        return this;
    }

    public BigBrotherResponse setData(D data) {
        this.data = data;
        return this;
    }

    public BigBrotherResponse setExtra(E extra) {
        this.extra = extra;
        return this;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public SupremeAuth getAuth() {
        return auth;
    }

    public D getData() {
        return data;
    }

    public E getExtra() {
        return extra;
    }

}
