package com.supreme.one.network.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Charlton on 3/1/18.
 */
public class SupremeAuth {
    @SerializedName("s1_token")
    String s1_token;
    @SerializedName("s1_token_acc")
    String s1_token_acc;

    public SupremeAuth() {
    }

    public SupremeAuth setS1Token(String s1_token) {
        this.s1_token = s1_token;
        return this;
    }

    public SupremeAuth setS1TokenAcc(String s1_token_acc) {
        this.s1_token_acc = s1_token_acc;
        return this;
    }

    public SupremeAuth(String s1_token, String s1_token_acc) {
        this.s1_token = s1_token;
        this.s1_token_acc = s1_token_acc;
    }

    public String getS1Token() {
        return s1_token;
    }

    public String getS1TokenAcc() {
        return s1_token_acc;
    }
}
