package com.supreme.one.network.client.models

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import androidx.annotation.ColorInt
import androidx.annotation.IntDef
import androidx.core.content.ContextCompat
import com.google.gson.annotations.SerializedName
import com.supreme.one.Supreme
import com.supreme.one.util.SupremeHelper
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 *
 * Actions <jas>/etc = A:msg
 * pub msg = C::
 * priv = P::
 * shout = S::
 * global buddy msg = G:: or G:
 */
open class Message : RealmObject() {


    @SerializedName("_cmd")
    var command: String? = null
    @SerializedName("inActivity")
    var inActivity: Boolean = false
    @SerializedName("from")
    var from: Int = 0
    @SerializedName("to")
    var to: Int = 0
    @SerializedName("message")
    var message: String? = null
    @ColorInt
    @SerializedName("fromPlayerColor")
    var fromPlayerColor: Int = 0
    @SerializedName("fromPlayerId")
    var fromPlayerId: Int = 0
    @SerializedName("fromPlayerName")
    var fromPlayerName: String? = null
    @ChatType
    @SerializedName("chatType")
    var chatType: Int = 0
    @PrimaryKey
    @SerializedName("createdAt")
    var createdAt: Long = Date().time


    private fun emoticons(context: Context?, messages: String?): SpannableString {

        val regex = "\\[(\\d+)\\]"
        val typeRegex = "(C:\\d+:)"
        val typeRegexNormal = "(C::)"
        val message = messages!!.replace("C::", "")
                .replace("&colon;",":")
                .replace(Regex(typeRegex), "")
        val spanMessage = SpannableString(message)
        message.replace(Regex(regex)) { result: MatchResult ->
            val value = result.value
            val icon = value.removeSurrounding("[", "]")
            val sticker = Supreme.STICKERS[icon]!!
            val drawable = ContextCompat.getDrawable(context!!, sticker)
            drawable!!.setBounds(0, 0, 50, 50)
            val imageSpan = ImageSpan(drawable, ImageSpan.ALIGN_BOTTOM)
            val spannableString = SpannableString(value)
            val indexOf = message.indexOf(value)
            spanMessage.setSpan(imageSpan, indexOf, indexOf + value.length, 0)
            return@replace spannableString
        }
        return spanMessage
    }


    fun filteredMessage(context: Context? = null): Spannable {
        return emoticons(context, message)
    }

    @ColorInt
    fun getContrastColor(colorIntValue: Int): Int {
        val red = Color.red(colorIntValue)
        val green = Color.green(colorIntValue)
        val blue = Color.blue(colorIntValue)
        val lum = 0.299 * red + (0.587 * green + 0.114 * blue)
        return if (lum > 186) 0xFF333333.toInt() else 0xFFFFFFFF.toInt()
    }


    fun getColorHexInt(): Int {
        return java.lang.Long.parseLong(if (fromPlayerColor == 0) "ff000000" else ("ff" + fromPlayerColor.toString(16)), 16).toInt()
    }

    fun getColorHexIntAlpha(): Int {
        return java.lang.Long.parseLong(if (fromPlayerColor == 0) "FF333333" else ("AA" + fromPlayerColor.toString(16)), 16).toInt()
    }

    fun getColor(): SupremeHelper.Color {
        return SupremeHelper.getColor(fromPlayerColor)
    }

    fun toHexColor(): String {
        val int = 0xFFFFFF and (fromPlayerColor)
        return String.format("#%06X", int)
    }

    @IntDef(PUBLIC, PRIVATE, SHOUT, GLOBAL)
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class ChatType
    companion object {
        const val PUBLIC = 0
        const val PRIVATE = 1
        const val SHOUT = 2
        const val GLOBAL: Int = 3
    }

    fun getAvatar(): String {
        return SupremeHelper.getAvatarUrlFromId(fromPlayerId)
    }

}
