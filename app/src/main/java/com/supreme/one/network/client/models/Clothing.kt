package com.supreme.one.network.client.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 3/3/18.
 */

open class Clothing : RealmObject() {
    var metaData: String? = null
    var filename: String? = null
    var gender: String? = null
    @PrimaryKey
    var id: Int = 0
    var version: Int = 0
    var categoryId: String? = null
}
