package com.supreme.one.network.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Charlton on 2/10/18.
 */
open class YoutubeResult: RealmObject() {
    @PrimaryKey
    var video_id: String? = null
    var channel_id: String? = null
    var title: String? = null
    var description: String? = null
    var channel: String? = null
    var etag: String? = null
    var thumbnail: String? = null
    var publish_at: Date? = null
    var relevance: Double? = null
    var video_link: String? = null
}