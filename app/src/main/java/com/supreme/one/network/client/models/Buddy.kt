package com.supreme.one.network.client.models

import com.supreme.one.util.SupremeHelper
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 3/4/18.
 */

open class Buddy : RealmObject() {
    var lastLogin: String? = null
    var getSyncedWith: Int = 0
    var instanceId: String? = null
    var savedCategoryId: String? = null
    var visibleRoomName: String? = null
    var onlineStatus: Int = 0
    var name: String? = null
    var isOnline: Boolean? = null
    var facebookName: String? = null
    var roomName: String? = null
    var categoryId: String? = null
    @PrimaryKey
    var playerId: Int = 0

    fun getAvatar(): String {
        return SupremeHelper.getAvatarUrlFromId(playerId)
    }
}
