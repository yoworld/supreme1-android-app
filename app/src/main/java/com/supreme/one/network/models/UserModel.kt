package com.supreme.one.network.models

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 1/30/18.
 */

open class UserModel : RealmObject() {

    @PrimaryKey
    var id: Int? = 0
    var name: String? = null
    var email: String? = null
    var active: Boolean? = false
    var user_banned: Int? = 0
    var level: Int? = 0
    var mod_title: String? = null
    var api_token: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var user_settings: YovilleAccountSettings? = null

}
