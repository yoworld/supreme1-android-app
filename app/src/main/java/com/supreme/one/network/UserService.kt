package com.supreme.one.network

import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.UserModel
import com.supreme.one.network.request.CredentialModel

import io.reactivex.Observable
import retrofit2.http.*

interface UserService {

    @get:Headers("Content-Type: application/json", "Accept: application/json")
    @get:GET("/api/v1/s1/user")
    val currentUser: Observable<YoResponse<UserModel, String>>

    //region Login/Register Credentials

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("/api/v1/auth/facebook")
    fun facebook(@Body token: CredentialModel): Observable<YoResponse<UserModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("/api/v1/auth/login")
    fun login(@Body login: CredentialModel): Observable<YoResponse<UserModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("/api/v1/auth/google")
    fun google(@Body token: CredentialModel): Observable<YoResponse<UserModel, PaginationData>>

    //endregion

}
