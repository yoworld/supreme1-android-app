package com.supreme.one.network.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class BuddyModel : RealmObject() {

    var user_id: Int? = 0
    var supreme_user_id: Int? = 0
    var user_name: String? = null
    var user_is_online: Int? = 0
    var user_room_name: String? = null
    var user_last_login: String? = null
    var category_id: Int? = 0
    var instance_id: Int? = 0
    var user_facebook_name: String? = null
    var user_facebook_id: Long? = 0
    var user_online_status: Int? = 0
    var saved_category_id: String? = null
    var visible_room_name: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var user_facebook_page: String? = null

    @PrimaryKey
    var composite_key: String? = null


}
