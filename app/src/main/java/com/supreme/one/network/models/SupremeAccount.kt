package com.supreme.one.network.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class SupremeAccount : RealmObject() {
    @PrimaryKey
    var user_id: Int = 0
    var user_name: String? = null
    var user_banned: Boolean = false
    var user_active: Boolean = false
    var user_ip: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var user_facebook_id: Long = 0
    var user_facebook_name: String? = null
    var user_avatar: String? = null
    var user_key: String? = null
    var user_level: Int = 0
    var user_settings: YovilleAccountSettings? = null
    var api_token: String? = null
    var assigned_to: Int? = 0
}
