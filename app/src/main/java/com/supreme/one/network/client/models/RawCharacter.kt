package com.supreme.one.network.client.models

import com.supreme.one.util.SupremeHelper
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Charlton on 3/3/18.
 */

open class RawCharacter: RealmObject() {
    var inWelcomeParty: Boolean = false
    var lastLogin: String? = null
    var maxEquippable: Int = 0
    var speechBalloonColor: String? = null
    var gender: Int = 0
    var badgeId: Int = 0
    var drunk_effect: String? = null
    var onlineStatus: Int = 0
    var locale: String? = null
    var createdOn: String? = null
    var followingPet: Long = 0
    var firstTime: Int = 0
    var entranceAction: String? = null
    var relationship_status: Int = 0
    var mod_level: Int = 0
    var name: String? = null
    @PrimaryKey
    var playerId: Int = 0
    var roomData: Room? = null
    var clothing: RealmList<Clothing>? = null
    var loggedByPlayerName: String? = null
    var loggedByPlayerId: Int = 0


    fun getAvatar(): String {
        return SupremeHelper.getAvatarUrlFromId(playerId)
    }
}
