package com.supreme.one.network

import com.supreme.one.network.models.CharacterModel
import com.supreme.one.network.models.PaginationData

import java.util.ArrayList
import java.util.HashMap

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.PATCH
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface CharacterService {


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/characters")
    fun search(@QueryMap query: HashMap<String, Any>): Observable<YoResponse<ArrayList<CharacterModel>, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("/api/v1/characters/{id}")
    fun update(@Path("id") id: Int, @Body query: HashMap<String, Any>): Observable<YoResponse<CharacterModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("/api/v1/characters/{id}")
    fun delete(@Path("id") id: Int): Observable<YoResponse<CharacterModel, PaginationData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/api/v1/characters/{id}")
    operator fun get(@Path("id") id: Int): Observable<YoResponse<CharacterModel, PaginationData>>


}
