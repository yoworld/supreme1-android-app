package com.supreme.one.network.request

/**
 * Created by Charlton on 1/30/18.
 */

class CredentialModel {

    var email: String? = null
    var password: String? = null
    var password_confirmation: String? = null
    var first_name: String? = null
    var last_name: String? = null
    var token: String? = null

    private constructor()

    private constructor(
            email: String,
            password: String
    ) {
        this.email = email
        this.password = password
    }

    private constructor(
            first_name: String,
            last_name: String,
            email: String,
            password: String
    ) {
        this.first_name = first_name
        this.last_name = last_name
        this.email = email
        this.password = password
    }

    private constructor(token: String) {
        this.token = token
    }

    companion object {
        fun token(token: String): CredentialModel {
            return CredentialModel(token)
        }
        fun register(first_name: String,
                     last_name: String,
                     email: String,
                     password: String): CredentialModel {
            return CredentialModel(first_name, last_name, email, password)
        }
        fun login(email: String,
                  password: String): CredentialModel {
            return CredentialModel(email, password)
        }
        fun reset(email: String,
                  password: String): CredentialModel {
            val credentialModel = CredentialModel(email, password)
            credentialModel.password_confirmation = password
            return credentialModel
        }
        fun forgot(email: String): CredentialModel {
            val credentialModel = CredentialModel()
            credentialModel.email = email
            return credentialModel
        }
    }
}