package com.supreme.one.network.models

import io.realm.RealmObject

open class CharacterModel : RealmObject() {
    var user_id: Int = 0
    var user_name: String? = null
    var user_gender: Int = 0
    var user_badge_id: Int = 0
    var user_last_login: String? = null
    var user_created_on: String? = null
    var first_time: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var logged_by_id: Int = 0
    var logged_by_name: String? = null
    var user_model_level: Int = 0
    var user_avatar: String? = null
    var user_network_id: String? = null
    var user_facebook_id: String? = null
    var user_type: String? = null
    var user_facebook_page: String? = null

}
