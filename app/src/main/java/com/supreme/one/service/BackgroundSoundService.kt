package com.supreme.one.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.IBinder
import timber.log.Timber
import java.io.IOException


/**
 * Created by Charlton on 2/11/18.
 */

class BackgroundSoundService : Service(), MediaPlayer.OnPreparedListener {
    internal var player: MediaPlayer? = null

    override fun onBind(arg0: Intent): IBinder? {
        return null
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.action?.equals("com.supreme.one.service.BackgroundSoundService.Exit") == true) {
            this.stopSelf()
            return START_NOT_STICKY
        } else {
            val uri = Uri.parse(intent.getStringExtra("url"))
            if (player == null) {
                player = MediaPlayer.create(this, uri)
                player?.setOnPreparedListener(this)
            } else {
                try {
                    if (player?.isPlaying == true) {
                        player?.stop()
                        player?.reset()
                    }
                    player?.setDataSource(this, uri)
                    player?.prepareAsync()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Timber.e(e, "BACKGROUNDSOUNDSERVICE ISSUE")
                }
            }
            return START_STICKY
        }
    }


    override fun onDestroy() {
        player?.stop()
        player?.release()
    }

    override fun onLowMemory() {

    }

    companion object {
        private val TAG = "MUSIC BACKGROUND SERVICE"


        fun startService(context: Context, url: String?) {
            val svc = Intent(context, BackgroundSoundService::class.java)
            svc.putExtra("url", url)
            context.startService(svc)
        }

        fun stopService(context: Context) {
            val exit = Intent("com.supreme.one.service.BackgroundSoundService.Exit") //intent filter has to be specified with the action name for the intent
            exit.setPackage("com.supreme.one")
            context.startService(exit)
        }
    }

    override fun onPrepared(mp: MediaPlayer?) {
        player?.start()
    }
}