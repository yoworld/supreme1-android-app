package com.supreme.one.base

import android.content.Context
import android.graphics.Rect
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.supreme.one.R
import com.supreme.one.Supreme
import com.supreme.one.network.models.UserModel
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * Created by Charlton on 2/8/18.
 */
open class BaseFragment : Fragment() {

    @Inject
    lateinit var sharedPreferenceManager: SharedPreferenceManager
    protected var disposables = CompositeDisposable()

    var mSnackbar: Snackbar? = null

    protected fun scrollToLatestMessage(recyclerView: RecyclerView, message: String, DELIM: Int = 10,
                                        @ColorInt color: Int = ContextCompat.getColor(recyclerView.context, R.color.white),
                                        @ColorInt bg: Int = ContextCompat.getColor(recyclerView.context, R.color.funny_blue)
                                        ) {
        val scrollBounds = Rect()
        recyclerView.getHitRect(scrollBounds)
        val view = recyclerView.findChildViewUnder(scrollBounds.centerX().toFloat(), scrollBounds.centerY().toFloat())
        if (view != null) {
            val index = recyclerView.getChildAdapterPosition(view)
            if (index > -1) {
                if (recyclerView.adapter!!.itemCount > index && index > recyclerView.adapter!!.itemCount - DELIM) {
                    recyclerView.smoothScrollToPosition(recyclerView.adapter!!.itemCount - 1)
                } else {
                    if (mSnackbar == null) {
                        mSnackbar = make(
                                recyclerView,
                                "New message: $message", Snackbar.LENGTH_SHORT,
                                bg,
                                color
                        )
                    } else {
                        mSnackbar!!.view.setBackgroundColor(bg)
                        mSnackbar!!.setActionTextColor(color)
                        val snackbarTextId = com.google.android.material.R.id.snackbar_text
                        val textView = mSnackbar!!.view.findViewById(snackbarTextId) as TextView
                        textView.setTextColor(color)
                        mSnackbar!!.setText("New message: ($message)")
                        mSnackbar!!.setAction("Show") { recyclerView.smoothScrollToPosition(recyclerView.adapter!!.itemCount - 1) }
                        mSnackbar!!.show()
                    }
                }
            }
        }
    }


    open fun onUserLoggedIn(user: UserModel) {

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        disposables.add(Supreme.subject.subscribe { onUserLoggedIn(it) })
    }


    override fun onDestroyView() {
        disposables.clear()
        super.onDestroyView()
    }


    fun make(view: View, string: String, time: Int, @ColorInt color: Int, @ColorInt actioncolor: Int): Snackbar {
        val snackbar = Snackbar.make(view, string, time)

        val snackbarTextId = com.google.android.material.R.id.snackbar_text
        val textView = snackbar.view.findViewById(snackbarTextId) as TextView
        textView.setTextColor(actioncolor)
        snackbar.view.setBackgroundColor(color)
        snackbar.setActionTextColor(actioncolor)
        return snackbar
    }

}