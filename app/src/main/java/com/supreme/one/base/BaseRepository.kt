package com.supreme.one.base

import com.supreme.one.util.SharedPreferenceManager

/**
 * Created by Charlton on 2/7/18.
 */
interface BaseRepository<T, S> {
    fun add(item: T)

    fun add(items: List<T>)

    fun update(item: T)

    fun remove(item: T)

    fun query(specification: S, queryable: HashMap<String, Any> = HashMap()): List<T>

    fun getSharedPreferenceManager() : SharedPreferenceManager
}