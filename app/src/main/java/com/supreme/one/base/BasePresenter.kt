package com.supreme.one.base

import com.supreme.one.App
import com.supreme.one.network.client.AppClient
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

open class BasePresenter<V> protected constructor(view: V) {

    private var disposables = CompositeDisposable()


    @Inject
    lateinit var sharedPreferenceManager: SharedPreferenceManager

    @Inject
    lateinit var mAppClient: AppClient


    fun start(){

    }

    fun stop(){
        disposables.clear()
    }

    protected fun addDisposable(disposable: Disposable){
        disposables.add(disposable)
    }

}