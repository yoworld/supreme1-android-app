package com.supreme.one.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.annotation.ColorInt
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.supreme.one.Supreme
import com.supreme.one.network.client.AppClient
import com.supreme.one.network.models.UserModel
import com.supreme.one.util.SharedPreferenceManager
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@SuppressLint("Registered")
/**
 * Created by Charlton on 2/7/18.
 */
open class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mAppClient: AppClient

    protected var disposables = CompositeDisposable()

    @Inject
    lateinit var sharedPreferenceManager: SharedPreferenceManager


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun make(view: View, string: String, time: Int, @ColorInt color: Int, @ColorInt actioncolor: Int) : Snackbar {
        val snackbar = Snackbar.make(view, string, time)
        snackbar.view.setBackgroundColor(color)
        snackbar.setActionTextColor(actioncolor)
        return snackbar
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        disposables.add(Supreme.subject.subscribe { onUserLoggedIn(it) })
        disposables.add(Supreme.connectionConnected.observeOn(AndroidSchedulers.mainThread()).subscribe {
            onConnected(it)
        })
        disposables.add(Supreme.connectionRetry.observeOn(AndroidSchedulers.mainThread()).subscribe {
            onRetrying(it)
        })
        disposables.add(Supreme.connectionDisconnected.observeOn(AndroidSchedulers.mainThread()).subscribe {
            onDisconnected(it)
        })
        disposables.add(Supreme.connectSwf.observeOn(AndroidSchedulers.mainThread()).subscribe {
            onConnectSwfMessage(it)
        })

    }

    protected open fun onConnectSwfMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected open fun onConnected(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected open fun onRetrying(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected open fun onDisconnected(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        disposables.clear()
        super.onStop()
    }

    open fun onUserLoggedIn(user: UserModel?) {

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

}