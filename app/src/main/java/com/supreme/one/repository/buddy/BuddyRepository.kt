package com.supreme.one.repository.buddy

import com.supreme.one.base.BaseRepository
import com.supreme.one.network.BuddyService
import com.supreme.one.network.models.BuddyModel

/**
 * Created by Charlton on 2/8/18.
 */
interface BuddyRepository: BaseRepository<BuddyModel, BuddySpecification>, BuddyService