package com.supreme.one.repository

import com.supreme.one.base.BaseSpecification
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults

/**
 * Created by Charlton on 2/7/18.
 */
interface RealmSpecification<T : RealmModel> : BaseSpecification {
    fun toRealmResults(realm: Realm): RealmResults<T>
    fun toRealmResults(realm: Realm, queryable: HashMap<String, Any>): RealmResults<T>
}