package com.supreme.one.repository.supreme

import com.supreme.one.network.models.SupremeAccount
import com.supreme.one.repository.RealmSpecification
import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class SupremeSpecification : RealmSpecification<SupremeAccount> {

    override fun toRealmResults(realm: Realm, queryable: HashMap<String, Any>): RealmResults<SupremeAccount> {
        var query = realm.where(SupremeAccount::class.java)
        for(i in queryable){
            when {

                i.value is Int -> query = query.equalTo(i.key, i.value as Int)
                i.value is String -> query = query.contains(i.key, i.value as String, Case.INSENSITIVE)
                i.value is Boolean -> query = query.equalTo(i.key, i.value as Boolean)
                i.value is Float -> query = query.equalTo(i.key, i.value as Float)
                i.value is Double -> query = query.equalTo(i.key, i.value as Double)
            }
        }
        return query.findAll()
    }

    override fun toRealmResults(realm: Realm): RealmResults<SupremeAccount> {
        return realm.where(SupremeAccount::class.java).findAll()
    }

}
