package com.supreme.one.repository.buddy

import com.supreme.one.network.models.BuddyModel
import com.supreme.one.repository.RealmSpecification
import io.realm.Case

import java.util.HashMap

import io.realm.Realm
import io.realm.RealmResults

/**
 * Created by Charlton on 2/7/18.
 */

class BuddySpecification : RealmSpecification<BuddyModel> {

    override fun toRealmResults(realm: Realm, queryable: HashMap<String, Any>): RealmResults<BuddyModel> {
        var query = realm.where(BuddyModel::class.java)
        for(i in queryable){
            when {

                i.value is Int -> query = query.equalTo(i.key, i.value as Int)
                i.value is String -> query = query.contains(i.key, i.value as String, Case.INSENSITIVE)
                i.value is Boolean -> query = query.equalTo(i.key, i.value as Boolean)
                i.value is Float -> query = query.equalTo(i.key, i.value as Float)
                i.value is Double -> query = query.equalTo(i.key, i.value as Double)
            }
        }
        return query.findAll()
    }

    override fun toRealmResults(realm: Realm): RealmResults<BuddyModel> {
        return realm.where(BuddyModel::class.java).findAll()
    }

}
