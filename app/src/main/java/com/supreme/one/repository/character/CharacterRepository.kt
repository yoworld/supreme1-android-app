package com.supreme.one.repository.character

import com.supreme.one.base.BaseRepository
import com.supreme.one.network.CharacterService
import com.supreme.one.network.UserService
import com.supreme.one.network.models.CharacterModel
import com.supreme.one.network.models.UserModel

/**
 * Created by Charlton on 2/8/18.
 */
interface CharacterRepository : BaseRepository<CharacterModel, CharacterSpecification>, CharacterService