package com.supreme.one.repository.soundcloud

import com.supreme.one.Supreme
import com.supreme.one.network.SoundCloudService
import com.supreme.one.network.YoResponse
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.SoundCloudPlaylistModel
import com.supreme.one.network.models.SoundCloudResult
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import okhttp3.ResponseBody
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class SoundCloudRepositoryImpl(private val userService: SoundCloudService, private val mSharedPreferenceManager: SharedPreferenceManager) : SoundCloudRepository {

    override fun getSharedPreferenceManager(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }

    override fun findSongs(query: String, page: Int, per: Int): Observable<YoResponse<ArrayList<SoundCloudResult>, PaginationData>> {
        return userService.findSongs(query, page, per).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { response ->
                    run {
                        val defaultInstance = Realm.getDefaultInstance()
                        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(response.data) }
                        Supreme.music.onNext(response.data!!)
                    }
                }
    }

    override fun getCurrentPlaylist(): Observable<YoResponse<ArrayList<SoundCloudPlaylistModel>, PaginationData>> {
        return userService.getCurrentPlaylist().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }


    override fun getSoundCloudSong(query: String): Observable<ResponseBody> {
        return userService.getSoundCloudSong(query).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {

                }
    }

    override fun getUserPlaylist(id: Int, page: Int, per: Int): Observable<YoResponse<ArrayList<SoundCloudPlaylistModel>, PaginationData>> {
        return userService.getUserPlaylist(id, page, per).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun removeSong(id: Int, track_id: Int): Observable<YoResponse<String, String>> {
        return userService.removeSong(id, track_id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    val first = query(SoundCloudSpecification(), object : HashMap<String, Any>() {
                        init {
                            put("user_id", id)
                            put("track_id", track_id)
                        }
                    }).first()
                    if (first != null) {
                        remove(first)
                    }
                }
    }

    override fun updateSong(id: Int, track_id: Int, map: HashMap<String, Any>): Observable<YoResponse<SoundCloudPlaylistModel, PaginationData>> {
        return userService.updateSong(id, track_id, map).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    update(it.data!!)
                }
    }

    override fun addSong(id: Int, map: HashMap<String, Any>): Observable<YoResponse<SoundCloudPlaylistModel, PaginationData>> {
        return userService.addSong(id, map).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun add(item: SoundCloudPlaylistModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun add(items: List<SoundCloudPlaylistModel>) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(items) }
    }

    override fun update(item: SoundCloudPlaylistModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun remove(item: SoundCloudPlaylistModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { item.deleteFromRealm() }
    }


    override fun query(specification: SoundCloudSpecification, queryable: HashMap<String, Any>): List<SoundCloudPlaylistModel> {
        return specification.toRealmResults(Realm.getDefaultInstance(), queryable)
    }

}