package com.supreme.one.repository.youtube

import com.supreme.one.base.BaseRepository
import com.supreme.one.network.UserService
import com.supreme.one.network.YoutubeService
import com.supreme.one.network.models.UserModel
import com.supreme.one.network.models.YoutubePlaylistModel

/**
 * Created by Charlton on 2/8/18.
 */
interface YoutubeRepository : BaseRepository<YoutubePlaylistModel, YoutubeSpecification>, YoutubeService