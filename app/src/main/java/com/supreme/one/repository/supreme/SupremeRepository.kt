package com.supreme.one.repository.supreme

import com.supreme.one.base.BaseRepository
import com.supreme.one.network.SupremeService
import com.supreme.one.network.models.SupremeAccount

/**
 * Created by Charlton on 2/8/18.
 */
interface SupremeRepository : BaseRepository<SupremeAccount, SupremeSpecification>, SupremeService