package com.supreme.one.repository.soundcloud

import com.supreme.one.base.BaseRepository
import com.supreme.one.network.SoundCloudService
import com.supreme.one.network.models.SoundCloudPlaylistModel

/**
 * Created by Charlton on 2/8/18.
 */
interface SoundCloudRepository : BaseRepository<SoundCloudPlaylistModel, SoundCloudSpecification>, SoundCloudService