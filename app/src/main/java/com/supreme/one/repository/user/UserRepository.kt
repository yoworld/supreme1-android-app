package com.supreme.one.repository.user

import com.supreme.one.base.BaseRepository
import com.supreme.one.network.UserService
import com.supreme.one.network.models.UserModel

/**
 * Created by Charlton on 2/8/18.
 */
interface UserRepository : BaseRepository<UserModel, UserSpecification>, UserService