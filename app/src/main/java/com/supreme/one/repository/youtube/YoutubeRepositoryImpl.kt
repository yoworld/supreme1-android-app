package com.supreme.one.repository.youtube

import com.supreme.one.Supreme
import com.supreme.one.network.YoResponse
import com.supreme.one.network.YoutubeService
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.YoutubeInfoModel
import com.supreme.one.network.models.YoutubePlaylistModel
import com.supreme.one.network.models.YoutubeResult
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import okhttp3.ResponseBody
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class YoutubeRepositoryImpl(private val userService: YoutubeService, private val mSharedPreferenceManager: SharedPreferenceManager) : YoutubeRepository {

    override fun getSharedPreferenceManager(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }

    override fun findVideos(query: String, page: Int, per: Int): Observable<YoResponse<ArrayList<YoutubeResult>, PaginationData>> {
        return userService.findVideos(query, page, per).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { response ->
                    run {
                        val defaultInstance = Realm.getDefaultInstance()
                        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(response.data) }
                        Supreme.video.onNext(response.data!!)
                    }
                }
    }

    override fun getCurrentPlaylist(): Observable<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>> {
        return userService.getCurrentPlaylist().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun getSongs(query: String, page: Int, per: Int): Observable<YoResponse<ArrayList<YoutubeInfoModel>, PaginationData>> {
        return userService.getSongs(query, page, per).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { model ->
                    run {
                        val defaultInstance = Realm.getDefaultInstance()
                        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(model.data) }
                    }
                }
    }

    override fun getYoutubeSong(query: String): Observable<ResponseBody> {
        return userService.getYoutubeSong(query).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {

                }
    }

    override fun getUserPlaylist(id: Int, page: Int, per: Int): Observable<YoResponse<ArrayList<YoutubePlaylistModel>, PaginationData>> {
        return userService.getUserPlaylist(id, page, per).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun removeSong(id: Int, vid: String): Observable<YoResponse<String, String>> {
        return userService.removeSong(id, vid).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    val first = query(YoutubeSpecification(), object : HashMap<String, Any>() {
                        init {
                            put("user_id", id)
                            put("youtube_id", vid)
                        }
                    }).first()
                    if (first != null) {
                        remove(first)
                    }
                }
    }

    override fun updateSong(id: Int, vid: String, map: HashMap<String, Any>): Observable<YoResponse<YoutubePlaylistModel, PaginationData>> {
        return userService.updateSong(id, vid, map).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    update(it.data!!)
                }
    }

    override fun addSong(id: Int, map: HashMap<String, Any>): Observable<YoResponse<YoutubePlaylistModel, PaginationData>> {
        return userService.addSong(id, map).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun add(item: YoutubePlaylistModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun add(items: List<YoutubePlaylistModel>) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(items) }
    }

    override fun update(item: YoutubePlaylistModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun remove(item: YoutubePlaylistModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { item.deleteFromRealm() }
    }


    override fun query(specification: YoutubeSpecification, queryable: HashMap<String, Any>): List<YoutubePlaylistModel> {
        return specification.toRealmResults(Realm.getDefaultInstance(), queryable)
    }

}