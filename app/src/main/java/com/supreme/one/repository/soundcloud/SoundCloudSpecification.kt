package com.supreme.one.repository.soundcloud

import com.supreme.one.network.models.SoundCloudPlaylistModel
import com.supreme.one.repository.RealmSpecification
import io.realm.Case

import java.util.HashMap

import io.realm.Realm
import io.realm.RealmResults

/**
 * Created by Charlton on 2/7/18.
 */

class SoundCloudSpecification : RealmSpecification<SoundCloudPlaylistModel> {

    override fun toRealmResults(realm: Realm, queryable: HashMap<String, Any>): RealmResults<SoundCloudPlaylistModel> {
        var query = realm.where(SoundCloudPlaylistModel::class.java)
        for(i in queryable){
            when (i.value) {
                is Int -> query = query.equalTo(i.key, i.value as Int)
                is String -> query = query.contains(i.key, i.value as String, Case.INSENSITIVE)
                is Boolean -> query = query.equalTo(i.key, i.value as Boolean)
                is Float -> query = query.equalTo(i.key, i.value as Float)
                is Double -> query = query.equalTo(i.key, i.value as Double)
            }
        }
        return query.findAll()
    }

    override fun toRealmResults(realm: Realm): RealmResults<SoundCloudPlaylistModel> {
        return realm.where(SoundCloudPlaylistModel::class.java).findAll()
    }

}
