package com.supreme.one.repository.user

import com.supreme.one.Supreme
import com.supreme.one.network.UserService
import com.supreme.one.network.YoResponse
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.UserModel
import com.supreme.one.network.request.CredentialModel
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import timber.log.Timber
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class UserRepositoryImpl(private val userService: UserService, private val mSharedPreferenceManager: SharedPreferenceManager) : UserRepository {

    override fun getSharedPreferenceManager(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }

    override fun add(item: UserModel) {
        try {
            Timber.e("User: %s, %s", item.name, item.id)
            val defaultInstance = Realm.getDefaultInstance()
            defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
        } catch (e: Exception) {
            Timber.e(e, "ERROR: %s", e.message)
        }
    }

    override fun add(items: List<UserModel>) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(items) }
    }

    override fun update(item: UserModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun remove(item: UserModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { item.deleteFromRealm() }
    }


    override fun query(specification: UserSpecification, queryable: HashMap<String, Any>): List<UserModel> {
        return specification.toRealmResults(Realm.getDefaultInstance(), queryable)
    }

    override val currentUser: Observable<YoResponse<UserModel, String>>
        get() = userService.currentUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturnItem(YoResponse(true, "", mSharedPreferenceManager.getUser()!!, ""))
                .doOnNext {
                    Timber.e("YO BITCH")
                    if(!it.data!!.isManaged) {
                        add(it.data!!)
                    }
                    mSharedPreferenceManager.saveUser(it.data!!)
                    mSharedPreferenceManager.saveId(it.data!!.id!!)
                    Supreme.subject.onNext(it.data!!)
                }


    override fun facebook(token: CredentialModel): Observable<YoResponse<UserModel, PaginationData>> {
        return userService.facebook(token).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                    mSharedPreferenceManager.saveUser(it.data!!)
                    mSharedPreferenceManager.saveId(it.data!!.id!!)
                    mSharedPreferenceManager.saveToken(it.data!!.api_token!!)
                    Supreme.subject.onNext(it.data!!)
                }
    }

    override fun login(login: CredentialModel): Observable<YoResponse<UserModel, PaginationData>> {
        return userService.login(login).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                    mSharedPreferenceManager.saveUser(it.data!!)
                    mSharedPreferenceManager.saveId(it.data!!.id!!)
                    mSharedPreferenceManager.saveToken(it.data!!.api_token!!)
                    Supreme.subject.onNext(it.data!!)
                }
    }

    override fun google(token: CredentialModel): Observable<YoResponse<UserModel, PaginationData>> {
        return userService.google(token).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                    mSharedPreferenceManager.saveUser(it.data!!)
                    mSharedPreferenceManager.saveId(it.data!!.id!!)
                    mSharedPreferenceManager.saveToken(it.data!!.api_token!!)
                    Supreme.subject.onNext(it.data!!)
                }
    }
}