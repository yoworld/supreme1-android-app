package com.supreme.one.repository.buddy

import com.supreme.one.network.BuddyService
import com.supreme.one.network.YoResponse
import com.supreme.one.network.models.BuddyModel
import com.supreme.one.network.models.PaginationData
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class BuddyRepositoryImpl(private val userService: BuddyService, private val mSharedPreferenceManager: SharedPreferenceManager) : BuddyRepository {
    override fun getSharedPreferenceManager(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }

    override fun search(id: Int, query: HashMap<String, Any>): Observable<YoResponse<ArrayList<BuddyModel>, PaginationData>> {
        return userService.search(id, query).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun update(id: Int, buddy_id: Int, query: HashMap<String, Any>): Observable<YoResponse<BuddyModel, PaginationData>> {
        return userService.update(id, buddy_id, query).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    update(it.data!!)
                }
    }

    override fun delete(user_id: Int, buddy_id: Int): Observable<YoResponse<BuddyModel, PaginationData>> {
        return userService.delete(user_id, buddy_id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    remove(it.data!!)
                }
    }

    override fun get(user_id: Int, buddy_id: Int): Observable<YoResponse<BuddyModel, PaginationData>> {
        return userService[user_id, buddy_id].subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }


    override fun add(item: BuddyModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun add(items: List<BuddyModel>) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(items) }
    }

    override fun update(item: BuddyModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun remove(item: BuddyModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { item.deleteFromRealm() }
    }

    override fun query(specification: BuddySpecification, queryable: HashMap<String, Any>): List<BuddyModel> {
        return specification.toRealmResults(Realm.getDefaultInstance(), queryable)
    }

}