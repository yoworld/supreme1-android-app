package com.supreme.one.repository.supreme

import com.supreme.one.network.SupremeService
import com.supreme.one.network.YoResponse
import com.supreme.one.network.models.PaginationData
import com.supreme.one.network.models.SupremeAccount
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class SupremeRepositoryImpl(private val userService: SupremeService, private val mSharedPreferenceManager: SharedPreferenceManager) : SupremeRepository {
    override fun getSharedPreferenceManager(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }

    override fun usersAccount(id: Int): Observable<YoResponse<ArrayList<SupremeAccount>, PaginationData>> {
        return userService.usersAccount(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }

    }

    override fun get(id: Int): Observable<YoResponse<SupremeAccount, String>> {
        return userService[id].subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }


    override fun add(item: SupremeAccount) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun add(items: List<SupremeAccount>) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(items) }
    }

    override fun update(item: SupremeAccount) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun remove(item: SupremeAccount) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { item.deleteFromRealm() }
    }

    override fun query(specification: SupremeSpecification, queryable: HashMap<String, Any>): List<SupremeAccount> {
        return specification.toRealmResults(Realm.getDefaultInstance(), queryable)
    }

}