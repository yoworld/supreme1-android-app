package com.supreme.one.repository.character

import com.supreme.one.network.CharacterService
import com.supreme.one.network.YoResponse
import com.supreme.one.network.models.CharacterModel
import com.supreme.one.network.models.PaginationData
import com.supreme.one.util.SharedPreferenceManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.*

/**
 * Created by Charlton on 2/7/18.
 */

class CharacterRepositoryImpl(private val userService: CharacterService, private val mSharedPreferenceManager: SharedPreferenceManager) : CharacterRepository {
    override fun getSharedPreferenceManager(): SharedPreferenceManager {
        return mSharedPreferenceManager
    }
    override fun search(query: HashMap<String, Any>): Observable<YoResponse<ArrayList<CharacterModel>, PaginationData>> {
        return userService.search(query).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun update(id: Int, query: HashMap<String, Any>): Observable<YoResponse<CharacterModel, PaginationData>> {
        return userService.update(id, query).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    update(it.data!!)
                }
    }

    override fun delete(id: Int): Observable<YoResponse<CharacterModel, PaginationData>> {
        return userService.delete(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    val model = Realm.getDefaultInstance().where(CharacterModel::class.java).equalTo("user_id", id).findFirst()
                    if (model != null) {
                        remove(model)
                    }
                }
    }

    override fun get(id: Int): Observable<YoResponse<CharacterModel, PaginationData>> {
        return userService[id].subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    add(it.data!!)
                }
    }

    override fun add(item: CharacterModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun add(items: List<CharacterModel>) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(items) }
    }

    override fun update(item: CharacterModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { it.copyToRealmOrUpdate(item) }
    }

    override fun remove(item: CharacterModel) {
        val defaultInstance = Realm.getDefaultInstance()
        defaultInstance.executeTransaction { item.deleteFromRealm() }
    }


    override fun query(specification: CharacterSpecification, queryable: HashMap<String, Any>): List<CharacterModel> {
        return specification.toRealmResults(Realm.getDefaultInstance(), queryable)
    }


}