package com.supreme.one.repository.character

import com.supreme.one.network.models.UserModel
import com.supreme.one.network.models.CharacterModel
import com.supreme.one.repository.RealmSpecification
import io.realm.Case

import java.util.HashMap

import io.realm.Realm
import io.realm.RealmResults

/**
 * Created by Charlton on 2/7/18.
 */

class CharacterSpecification : RealmSpecification<CharacterModel> {

    override fun toRealmResults(realm: Realm, queryable: HashMap<String, Any>): RealmResults<CharacterModel> {
        var query = realm.where(CharacterModel::class.java)
        for(i in queryable){
            when {
                i.value is Int -> query = query.equalTo(i.key, i.value as Int)
                i.value is String -> query = query.contains(i.key, i.value as String, Case.INSENSITIVE)
                i.value is Boolean -> query = query.equalTo(i.key, i.value as Boolean)
                i.value is Float -> query = query.equalTo(i.key, i.value as Float)
                i.value is Double -> query = query.equalTo(i.key, i.value as Double)
            }
        }
        return query.findAll()
    }

    override fun toRealmResults(realm: Realm): RealmResults<CharacterModel> {
        return realm.where(CharacterModel::class.java).findAll()
    }

}
